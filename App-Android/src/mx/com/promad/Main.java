package mx.com.promad;
 
import java.rmi.RemoteException; 
import com.promad.movil.interfaces.IgenerarEmergenciaResponse;
import com.promad.movil.interfaces.IntegracionMovilInterface_ItemPersona;
import com.promad.movil.interfaces.IntegracionMovilInterface_ItemVehiculo;
import com.promad.movil.interfaces.IntegracionMovilPortTypeProxy;
import com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS;
import com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoapProxy;

public class Main {

	public static void main(String[] args) {
		 
		String in0=null;
		String in1="0000"; 
		long in2=0; 
		String in3="BOTON_APP"; 
		String in4="PRUEBA TEST SOAP"; 
		String in5="OTROS / BOTON APP MOVIL"; 
		String in6=null; 
		String in7=null; 
		String in8=null; 
		String in9=null; 
		String in10=null; 
		String in11=null; 
		String in12=null; 
		String in13="-99.09090404605016"; 
		String in14="19.372722378948992"; 
		String in15=null; 
		String in16=null; 
		String in17=null; 
		IntegracionMovilInterface_ItemVehiculo[] in18=null; 
		IntegracionMovilInterface_ItemPersona[] in19=null; 
		String in20="RAPIDA"; 
		// ----------------
		Object myObject =null; 
		IgenerarEmergenciaResponse MyResponse= new IgenerarEmergenciaResponse();
		Object[] myArryObj=null;
		// ----------------
		IntegracionMovilPortTypeProxy myProxy = new IntegracionMovilPortTypeProxy();
		try {
			myObject = myProxy.generarEmergencia(in0, in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20);
			System.out.println("consulta exitosa !!!!");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			System.out.println("Hubo un problema con la consulta");
			e.printStackTrace();
		}
		
		if(myObject instanceof Object[]){
			myArryObj = (Object[]) myObject;
			System.out.println("Si es instancia de Object[] !!!");
			MyResponse.getGenerarEmergenciaReturn()[0] = myArryObj[0].toString();
			MyResponse.getGenerarEmergenciaReturn()[1] = myArryObj[1].toString();
			MyResponse.getGenerarEmergenciaReturn()[2] = myArryObj[2].toString();
			System.out.println("Todos los datos recuperados !!!");
			for(String str:MyResponse.getGenerarEmergenciaReturn())
				System.out.println("Los Datos : <" + str + ">");
		}
		else {
			System.out.println("No es instancia de Object[]");
		}
		
	}
	
	public static void old_main(String[] args) {
		
 
		System.out.println("Hello World");
		
		LocomotivePositionRS Response = null;
		
		LocomotivePositionServiceSoapProxy myProxy = new LocomotivePositionServiceSoapProxy();
		try {
			Response = myProxy.getLocomotiveLatestPosition("", "");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Se registraron <" + Response.getLocomotives().length + "> Locomotoras");
		
	}

}
