/**
 * GeoFenceType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class GeoFenceType  implements java.io.Serializable {
    private java.lang.String fenceEvent;

    private java.lang.Integer fenceTypeId;

    private java.lang.String fenceCode;

    public GeoFenceType() {
    }

    public GeoFenceType(
           java.lang.String fenceEvent,
           java.lang.Integer fenceTypeId,
           java.lang.String fenceCode) {
           this.fenceEvent = fenceEvent;
           this.fenceTypeId = fenceTypeId;
           this.fenceCode = fenceCode;
    }


    /**
     * Gets the fenceEvent value for this GeoFenceType.
     * 
     * @return fenceEvent
     */
    public java.lang.String getFenceEvent() {
        return fenceEvent;
    }


    /**
     * Sets the fenceEvent value for this GeoFenceType.
     * 
     * @param fenceEvent
     */
    public void setFenceEvent(java.lang.String fenceEvent) {
        this.fenceEvent = fenceEvent;
    }


    /**
     * Gets the fenceTypeId value for this GeoFenceType.
     * 
     * @return fenceTypeId
     */
    public java.lang.Integer getFenceTypeId() {
        return fenceTypeId;
    }


    /**
     * Sets the fenceTypeId value for this GeoFenceType.
     * 
     * @param fenceTypeId
     */
    public void setFenceTypeId(java.lang.Integer fenceTypeId) {
        this.fenceTypeId = fenceTypeId;
    }


    /**
     * Gets the fenceCode value for this GeoFenceType.
     * 
     * @return fenceCode
     */
    public java.lang.String getFenceCode() {
        return fenceCode;
    }


    /**
     * Sets the fenceCode value for this GeoFenceType.
     * 
     * @param fenceCode
     */
    public void setFenceCode(java.lang.String fenceCode) {
        this.fenceCode = fenceCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GeoFenceType)) return false;
        GeoFenceType other = (GeoFenceType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fenceEvent==null && other.getFenceEvent()==null) || 
             (this.fenceEvent!=null &&
              this.fenceEvent.equals(other.getFenceEvent()))) &&
            ((this.fenceTypeId==null && other.getFenceTypeId()==null) || 
             (this.fenceTypeId!=null &&
              this.fenceTypeId.equals(other.getFenceTypeId()))) &&
            ((this.fenceCode==null && other.getFenceCode()==null) || 
             (this.fenceCode!=null &&
              this.fenceCode.equals(other.getFenceCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFenceEvent() != null) {
            _hashCode += getFenceEvent().hashCode();
        }
        if (getFenceTypeId() != null) {
            _hashCode += getFenceTypeId().hashCode();
        }
        if (getFenceCode() != null) {
            _hashCode += getFenceCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GeoFenceType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GeoFenceType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fenceEvent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "FenceEvent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fenceTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "FenceTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fenceCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "FenceCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
