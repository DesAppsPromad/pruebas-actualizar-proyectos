/**
 * LocomotivePositionServiceSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class LocomotivePositionServiceSoapStub extends org.apache.axis.client.Stub implements com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[8];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocomotiveLatestPositionByUnitNumber");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "username"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "initials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "unitNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setReturnClass(com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocomotiveLatestPosition");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "username"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setReturnClass(com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocomotivesPositionsByRecordtime");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "username"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setReturnClass(com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocomotivesPositionsByDateRange");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "username"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordStartTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordEndTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setReturnClass(com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocomotivesRapidSamplesByRecordtime");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "username"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordStartTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordEndTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setReturnClass(com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetRefuelsByDateRange");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "username"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordStartTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordEndTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setReturnClass(com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetEpisodesByDateRange");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "username"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordStartTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "recordEndTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "episodeTypes"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setReturnClass(com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetEpisodeTypes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "username"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "entireHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "languageID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "InfoRS"));
        oper.setReturnClass(com.witronix.www.WebServices.v002_003_000.InfoRS.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "InfoRS"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

    }

    public LocomotivePositionServiceSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public LocomotivePositionServiceSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public LocomotivePositionServiceSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ArrayOfEpisodeType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.EpisodeType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeType");
            qName2 = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Episode");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ArrayOfEpisodeTypeType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.EpisodeTypeType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeTypeType");
            qName2 = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ArrayOfErrorType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.ErrorType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ErrorType");
            qName2 = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Error");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ArrayOfInfoType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.InfoType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "InfoType");
            qName2 = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Info");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ArrayOfLocomotiveType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.LocomotiveType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotiveType");
            qName2 = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Locomotive");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ArrayOfPositionType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.PositionType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "PositionType");
            qName2 = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Position");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ArrayOfRefuelType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.RefuelType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "RefuelType");
            qName2 = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Refuel");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeDataType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.EpisodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.EpisodeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeTypeType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.EpisodeTypeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ErrorType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.ErrorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GeoFenceType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.GeoFenceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "InfoRS");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.InfoRS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "InfoType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.InfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotiveType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.LocomotiveType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "PositionType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.PositionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "RefuelType");
            cachedSerQNames.add(qName);
            cls = com.witronix.www.WebServices.v002_003_000.RefuelType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotiveLatestPositionByUnitNumber(java.lang.String username, java.lang.String password, java.lang.String initials, java.lang.String unitNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.witronix.com/WebServices/v002.003.000/GetLocomotiveLatestPositionByUnitNumber");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GetLocomotiveLatestPositionByUnitNumber"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {username, password, initials, unitNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) org.apache.axis.utils.JavaUtils.convert(_resp, com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotiveLatestPosition(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.witronix.com/WebServices/v002.003.000/GetLocomotiveLatestPosition");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GetLocomotiveLatestPosition"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {username, password});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) org.apache.axis.utils.JavaUtils.convert(_resp, com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesPositionsByRecordtime(java.lang.String username, java.lang.String password, java.util.Calendar recordTime) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.witronix.com/WebServices/v002.003.000/GetLocomotivesPositionsByRecordtime");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GetLocomotivesPositionsByRecordtime"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {username, password, recordTime});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) org.apache.axis.utils.JavaUtils.convert(_resp, com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesPositionsByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.witronix.com/WebServices/v002.003.000/GetLocomotivesPositionsByDateRange");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GetLocomotivesPositionsByDateRange"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {username, password, recordStartTime, recordEndTime});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) org.apache.axis.utils.JavaUtils.convert(_resp, com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesRapidSamplesByRecordtime(java.lang.String username, java.lang.String password, java.lang.String recordStartTime, java.lang.String recordEndTime) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.witronix.com/WebServices/v002.003.000/GetLocomotivesRapidSamplesByRecordtime");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GetLocomotivesRapidSamplesByRecordtime"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {username, password, recordStartTime, recordEndTime});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) org.apache.axis.utils.JavaUtils.convert(_resp, com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getRefuelsByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.witronix.com/WebServices/v002.003.000/GetRefuelsByDateRange");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GetRefuelsByDateRange"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {username, password, recordStartTime, recordEndTime});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) org.apache.axis.utils.JavaUtils.convert(_resp, com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getEpisodesByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime, java.lang.String episodeTypes) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.witronix.com/WebServices/v002.003.000/GetEpisodesByDateRange");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GetEpisodesByDateRange"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {username, password, recordStartTime, recordEndTime, episodeTypes});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS) org.apache.axis.utils.JavaUtils.convert(_resp, com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.witronix.www.WebServices.v002_003_000.InfoRS getEpisodeTypes(java.lang.String username, java.lang.String password, boolean entireHistory, int languageID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.witronix.com/WebServices/v002.003.000/GetEpisodeTypes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GetEpisodeTypes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {username, password, new java.lang.Boolean(entireHistory), new java.lang.Integer(languageID)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.witronix.www.WebServices.v002_003_000.InfoRS) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.witronix.www.WebServices.v002_003_000.InfoRS) org.apache.axis.utils.JavaUtils.convert(_resp, com.witronix.www.WebServices.v002_003_000.InfoRS.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
