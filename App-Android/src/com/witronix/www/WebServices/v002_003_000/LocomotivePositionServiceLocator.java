/**
 * LocomotivePositionServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class LocomotivePositionServiceLocator extends org.apache.axis.client.Service implements com.witronix.www.WebServices.v002_003_000.LocomotivePositionService {

    public LocomotivePositionServiceLocator() {
    }


    public LocomotivePositionServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LocomotivePositionServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LocomotivePositionServiceSoap
    private java.lang.String LocomotivePositionServiceSoap_address = "https://webservices.wi-tronix.com:448/LocomotivePositionService.asmx";

    public java.lang.String getLocomotivePositionServiceSoapAddress() {
        return LocomotivePositionServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LocomotivePositionServiceSoapWSDDServiceName = "LocomotivePositionServiceSoap";

    public java.lang.String getLocomotivePositionServiceSoapWSDDServiceName() {
        return LocomotivePositionServiceSoapWSDDServiceName;
    }

    public void setLocomotivePositionServiceSoapWSDDServiceName(java.lang.String name) {
        LocomotivePositionServiceSoapWSDDServiceName = name;
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap getLocomotivePositionServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LocomotivePositionServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLocomotivePositionServiceSoap(endpoint);
    }

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap getLocomotivePositionServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoapStub _stub = new com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoapStub(portAddress, this);
            _stub.setPortName(getLocomotivePositionServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLocomotivePositionServiceSoapEndpointAddress(java.lang.String address) {
        LocomotivePositionServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoapStub _stub = new com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoapStub(new java.net.URL(LocomotivePositionServiceSoap_address), this);
                _stub.setPortName(getLocomotivePositionServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LocomotivePositionServiceSoap".equals(inputPortName)) {
            return getLocomotivePositionServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LocomotivePositionServiceSoap".equals(portName)) {
            setLocomotivePositionServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
