/**
 * LocomotivePositionService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public interface LocomotivePositionService extends javax.xml.rpc.Service {
    public java.lang.String getLocomotivePositionServiceSoapAddress();

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap getLocomotivePositionServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap getLocomotivePositionServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
