package com.witronix.www.WebServices.v002_003_000;

public class LocomotivePositionServiceSoapProxy implements com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap {
  private String _endpoint = null;
  private com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap locomotivePositionServiceSoap = null;
  
  public LocomotivePositionServiceSoapProxy() {
    _initLocomotivePositionServiceSoapProxy();
  }
  
  public LocomotivePositionServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initLocomotivePositionServiceSoapProxy();
  }
  
  private void _initLocomotivePositionServiceSoapProxy() {
    try {
      locomotivePositionServiceSoap = (new com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceLocator()).getLocomotivePositionServiceSoap();
      if (locomotivePositionServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)locomotivePositionServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)locomotivePositionServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (locomotivePositionServiceSoap != null)
      ((javax.xml.rpc.Stub)locomotivePositionServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.witronix.www.WebServices.v002_003_000.LocomotivePositionServiceSoap getLocomotivePositionServiceSoap() {
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap;
  }
  
  public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotiveLatestPositionByUnitNumber(java.lang.String username, java.lang.String password, java.lang.String initials, java.lang.String unitNumber) throws java.rmi.RemoteException{
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap.getLocomotiveLatestPositionByUnitNumber(username, password, initials, unitNumber);
  }
  
  public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotiveLatestPosition(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException{
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap.getLocomotiveLatestPosition(username, password);
  }
  
  public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesPositionsByRecordtime(java.lang.String username, java.lang.String password, java.util.Calendar recordTime) throws java.rmi.RemoteException{
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap.getLocomotivesPositionsByRecordtime(username, password, recordTime);
  }
  
  public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesPositionsByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime) throws java.rmi.RemoteException{
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap.getLocomotivesPositionsByDateRange(username, password, recordStartTime, recordEndTime);
  }
  
  public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesRapidSamplesByRecordtime(java.lang.String username, java.lang.String password, java.lang.String recordStartTime, java.lang.String recordEndTime) throws java.rmi.RemoteException{
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap.getLocomotivesRapidSamplesByRecordtime(username, password, recordStartTime, recordEndTime);
  }
  
  public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getRefuelsByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime) throws java.rmi.RemoteException{
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap.getRefuelsByDateRange(username, password, recordStartTime, recordEndTime);
  }
  
  public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getEpisodesByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime, java.lang.String episodeTypes) throws java.rmi.RemoteException{
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap.getEpisodesByDateRange(username, password, recordStartTime, recordEndTime, episodeTypes);
  }
  
  public com.witronix.www.WebServices.v002_003_000.InfoRS getEpisodeTypes(java.lang.String username, java.lang.String password, boolean entireHistory, int languageID) throws java.rmi.RemoteException{
    if (locomotivePositionServiceSoap == null)
      _initLocomotivePositionServiceSoapProxy();
    return locomotivePositionServiceSoap.getEpisodeTypes(username, password, entireHistory, languageID);
  }
  
  
}