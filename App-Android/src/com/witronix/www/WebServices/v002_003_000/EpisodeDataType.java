/**
 * EpisodeDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class EpisodeDataType  implements java.io.Serializable {
    private java.lang.Double latitude;

    private java.lang.Double longitude;

    private java.lang.String location;

    private java.lang.String milePost;

    private java.math.BigDecimal milePostNumber;

    private java.lang.String subdivision;

    private java.lang.String railroad;

    private java.lang.Double direction;

    private java.lang.Float velocity;

    private com.witronix.www.WebServices.v002_003_000.GeoFenceType geoFence;

    private java.lang.Float fuelVolume;

    private java.lang.String throttlePosition;

    private java.lang.Integer engineRunningState;

    private java.lang.Float tractionMotorCurrent;

    private java.lang.Float tractionPower;

    private java.lang.Float BP_Pressure;

    private java.lang.Float BC_Pressure;

    private java.lang.Float EOT_BP_Pressure;

    public EpisodeDataType() {
    }

    public EpisodeDataType(
           java.lang.Double latitude,
           java.lang.Double longitude,
           java.lang.String location,
           java.lang.String milePost,
           java.math.BigDecimal milePostNumber,
           java.lang.String subdivision,
           java.lang.String railroad,
           java.lang.Double direction,
           java.lang.Float velocity,
           com.witronix.www.WebServices.v002_003_000.GeoFenceType geoFence,
           java.lang.Float fuelVolume,
           java.lang.String throttlePosition,
           java.lang.Integer engineRunningState,
           java.lang.Float tractionMotorCurrent,
           java.lang.Float tractionPower,
           java.lang.Float BP_Pressure,
           java.lang.Float BC_Pressure,
           java.lang.Float EOT_BP_Pressure) {
           this.latitude = latitude;
           this.longitude = longitude;
           this.location = location;
           this.milePost = milePost;
           this.milePostNumber = milePostNumber;
           this.subdivision = subdivision;
           this.railroad = railroad;
           this.direction = direction;
           this.velocity = velocity;
           this.geoFence = geoFence;
           this.fuelVolume = fuelVolume;
           this.throttlePosition = throttlePosition;
           this.engineRunningState = engineRunningState;
           this.tractionMotorCurrent = tractionMotorCurrent;
           this.tractionPower = tractionPower;
           this.BP_Pressure = BP_Pressure;
           this.BC_Pressure = BC_Pressure;
           this.EOT_BP_Pressure = EOT_BP_Pressure;
    }


    /**
     * Gets the latitude value for this EpisodeDataType.
     * 
     * @return latitude
     */
    public java.lang.Double getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this EpisodeDataType.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.Double latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this EpisodeDataType.
     * 
     * @return longitude
     */
    public java.lang.Double getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this EpisodeDataType.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.Double longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the location value for this EpisodeDataType.
     * 
     * @return location
     */
    public java.lang.String getLocation() {
        return location;
    }


    /**
     * Sets the location value for this EpisodeDataType.
     * 
     * @param location
     */
    public void setLocation(java.lang.String location) {
        this.location = location;
    }


    /**
     * Gets the milePost value for this EpisodeDataType.
     * 
     * @return milePost
     */
    public java.lang.String getMilePost() {
        return milePost;
    }


    /**
     * Sets the milePost value for this EpisodeDataType.
     * 
     * @param milePost
     */
    public void setMilePost(java.lang.String milePost) {
        this.milePost = milePost;
    }


    /**
     * Gets the milePostNumber value for this EpisodeDataType.
     * 
     * @return milePostNumber
     */
    public java.math.BigDecimal getMilePostNumber() {
        return milePostNumber;
    }


    /**
     * Sets the milePostNumber value for this EpisodeDataType.
     * 
     * @param milePostNumber
     */
    public void setMilePostNumber(java.math.BigDecimal milePostNumber) {
        this.milePostNumber = milePostNumber;
    }


    /**
     * Gets the subdivision value for this EpisodeDataType.
     * 
     * @return subdivision
     */
    public java.lang.String getSubdivision() {
        return subdivision;
    }


    /**
     * Sets the subdivision value for this EpisodeDataType.
     * 
     * @param subdivision
     */
    public void setSubdivision(java.lang.String subdivision) {
        this.subdivision = subdivision;
    }


    /**
     * Gets the railroad value for this EpisodeDataType.
     * 
     * @return railroad
     */
    public java.lang.String getRailroad() {
        return railroad;
    }


    /**
     * Sets the railroad value for this EpisodeDataType.
     * 
     * @param railroad
     */
    public void setRailroad(java.lang.String railroad) {
        this.railroad = railroad;
    }


    /**
     * Gets the direction value for this EpisodeDataType.
     * 
     * @return direction
     */
    public java.lang.Double getDirection() {
        return direction;
    }


    /**
     * Sets the direction value for this EpisodeDataType.
     * 
     * @param direction
     */
    public void setDirection(java.lang.Double direction) {
        this.direction = direction;
    }


    /**
     * Gets the velocity value for this EpisodeDataType.
     * 
     * @return velocity
     */
    public java.lang.Float getVelocity() {
        return velocity;
    }


    /**
     * Sets the velocity value for this EpisodeDataType.
     * 
     * @param velocity
     */
    public void setVelocity(java.lang.Float velocity) {
        this.velocity = velocity;
    }


    /**
     * Gets the geoFence value for this EpisodeDataType.
     * 
     * @return geoFence
     */
    public com.witronix.www.WebServices.v002_003_000.GeoFenceType getGeoFence() {
        return geoFence;
    }


    /**
     * Sets the geoFence value for this EpisodeDataType.
     * 
     * @param geoFence
     */
    public void setGeoFence(com.witronix.www.WebServices.v002_003_000.GeoFenceType geoFence) {
        this.geoFence = geoFence;
    }


    /**
     * Gets the fuelVolume value for this EpisodeDataType.
     * 
     * @return fuelVolume
     */
    public java.lang.Float getFuelVolume() {
        return fuelVolume;
    }


    /**
     * Sets the fuelVolume value for this EpisodeDataType.
     * 
     * @param fuelVolume
     */
    public void setFuelVolume(java.lang.Float fuelVolume) {
        this.fuelVolume = fuelVolume;
    }


    /**
     * Gets the throttlePosition value for this EpisodeDataType.
     * 
     * @return throttlePosition
     */
    public java.lang.String getThrottlePosition() {
        return throttlePosition;
    }


    /**
     * Sets the throttlePosition value for this EpisodeDataType.
     * 
     * @param throttlePosition
     */
    public void setThrottlePosition(java.lang.String throttlePosition) {
        this.throttlePosition = throttlePosition;
    }


    /**
     * Gets the engineRunningState value for this EpisodeDataType.
     * 
     * @return engineRunningState
     */
    public java.lang.Integer getEngineRunningState() {
        return engineRunningState;
    }


    /**
     * Sets the engineRunningState value for this EpisodeDataType.
     * 
     * @param engineRunningState
     */
    public void setEngineRunningState(java.lang.Integer engineRunningState) {
        this.engineRunningState = engineRunningState;
    }


    /**
     * Gets the tractionMotorCurrent value for this EpisodeDataType.
     * 
     * @return tractionMotorCurrent
     */
    public java.lang.Float getTractionMotorCurrent() {
        return tractionMotorCurrent;
    }


    /**
     * Sets the tractionMotorCurrent value for this EpisodeDataType.
     * 
     * @param tractionMotorCurrent
     */
    public void setTractionMotorCurrent(java.lang.Float tractionMotorCurrent) {
        this.tractionMotorCurrent = tractionMotorCurrent;
    }


    /**
     * Gets the tractionPower value for this EpisodeDataType.
     * 
     * @return tractionPower
     */
    public java.lang.Float getTractionPower() {
        return tractionPower;
    }


    /**
     * Sets the tractionPower value for this EpisodeDataType.
     * 
     * @param tractionPower
     */
    public void setTractionPower(java.lang.Float tractionPower) {
        this.tractionPower = tractionPower;
    }


    /**
     * Gets the BP_Pressure value for this EpisodeDataType.
     * 
     * @return BP_Pressure
     */
    public java.lang.Float getBP_Pressure() {
        return BP_Pressure;
    }


    /**
     * Sets the BP_Pressure value for this EpisodeDataType.
     * 
     * @param BP_Pressure
     */
    public void setBP_Pressure(java.lang.Float BP_Pressure) {
        this.BP_Pressure = BP_Pressure;
    }


    /**
     * Gets the BC_Pressure value for this EpisodeDataType.
     * 
     * @return BC_Pressure
     */
    public java.lang.Float getBC_Pressure() {
        return BC_Pressure;
    }


    /**
     * Sets the BC_Pressure value for this EpisodeDataType.
     * 
     * @param BC_Pressure
     */
    public void setBC_Pressure(java.lang.Float BC_Pressure) {
        this.BC_Pressure = BC_Pressure;
    }


    /**
     * Gets the EOT_BP_Pressure value for this EpisodeDataType.
     * 
     * @return EOT_BP_Pressure
     */
    public java.lang.Float getEOT_BP_Pressure() {
        return EOT_BP_Pressure;
    }


    /**
     * Sets the EOT_BP_Pressure value for this EpisodeDataType.
     * 
     * @param EOT_BP_Pressure
     */
    public void setEOT_BP_Pressure(java.lang.Float EOT_BP_Pressure) {
        this.EOT_BP_Pressure = EOT_BP_Pressure;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EpisodeDataType)) return false;
        EpisodeDataType other = (EpisodeDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation()))) &&
            ((this.milePost==null && other.getMilePost()==null) || 
             (this.milePost!=null &&
              this.milePost.equals(other.getMilePost()))) &&
            ((this.milePostNumber==null && other.getMilePostNumber()==null) || 
             (this.milePostNumber!=null &&
              this.milePostNumber.equals(other.getMilePostNumber()))) &&
            ((this.subdivision==null && other.getSubdivision()==null) || 
             (this.subdivision!=null &&
              this.subdivision.equals(other.getSubdivision()))) &&
            ((this.railroad==null && other.getRailroad()==null) || 
             (this.railroad!=null &&
              this.railroad.equals(other.getRailroad()))) &&
            ((this.direction==null && other.getDirection()==null) || 
             (this.direction!=null &&
              this.direction.equals(other.getDirection()))) &&
            ((this.velocity==null && other.getVelocity()==null) || 
             (this.velocity!=null &&
              this.velocity.equals(other.getVelocity()))) &&
            ((this.geoFence==null && other.getGeoFence()==null) || 
             (this.geoFence!=null &&
              this.geoFence.equals(other.getGeoFence()))) &&
            ((this.fuelVolume==null && other.getFuelVolume()==null) || 
             (this.fuelVolume!=null &&
              this.fuelVolume.equals(other.getFuelVolume()))) &&
            ((this.throttlePosition==null && other.getThrottlePosition()==null) || 
             (this.throttlePosition!=null &&
              this.throttlePosition.equals(other.getThrottlePosition()))) &&
            ((this.engineRunningState==null && other.getEngineRunningState()==null) || 
             (this.engineRunningState!=null &&
              this.engineRunningState.equals(other.getEngineRunningState()))) &&
            ((this.tractionMotorCurrent==null && other.getTractionMotorCurrent()==null) || 
             (this.tractionMotorCurrent!=null &&
              this.tractionMotorCurrent.equals(other.getTractionMotorCurrent()))) &&
            ((this.tractionPower==null && other.getTractionPower()==null) || 
             (this.tractionPower!=null &&
              this.tractionPower.equals(other.getTractionPower()))) &&
            ((this.BP_Pressure==null && other.getBP_Pressure()==null) || 
             (this.BP_Pressure!=null &&
              this.BP_Pressure.equals(other.getBP_Pressure()))) &&
            ((this.BC_Pressure==null && other.getBC_Pressure()==null) || 
             (this.BC_Pressure!=null &&
              this.BC_Pressure.equals(other.getBC_Pressure()))) &&
            ((this.EOT_BP_Pressure==null && other.getEOT_BP_Pressure()==null) || 
             (this.EOT_BP_Pressure!=null &&
              this.EOT_BP_Pressure.equals(other.getEOT_BP_Pressure())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        if (getMilePost() != null) {
            _hashCode += getMilePost().hashCode();
        }
        if (getMilePostNumber() != null) {
            _hashCode += getMilePostNumber().hashCode();
        }
        if (getSubdivision() != null) {
            _hashCode += getSubdivision().hashCode();
        }
        if (getRailroad() != null) {
            _hashCode += getRailroad().hashCode();
        }
        if (getDirection() != null) {
            _hashCode += getDirection().hashCode();
        }
        if (getVelocity() != null) {
            _hashCode += getVelocity().hashCode();
        }
        if (getGeoFence() != null) {
            _hashCode += getGeoFence().hashCode();
        }
        if (getFuelVolume() != null) {
            _hashCode += getFuelVolume().hashCode();
        }
        if (getThrottlePosition() != null) {
            _hashCode += getThrottlePosition().hashCode();
        }
        if (getEngineRunningState() != null) {
            _hashCode += getEngineRunningState().hashCode();
        }
        if (getTractionMotorCurrent() != null) {
            _hashCode += getTractionMotorCurrent().hashCode();
        }
        if (getTractionPower() != null) {
            _hashCode += getTractionPower().hashCode();
        }
        if (getBP_Pressure() != null) {
            _hashCode += getBP_Pressure().hashCode();
        }
        if (getBC_Pressure() != null) {
            _hashCode += getBC_Pressure().hashCode();
        }
        if (getEOT_BP_Pressure() != null) {
            _hashCode += getEOT_BP_Pressure().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EpisodeDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Location"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("milePost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "MilePost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("milePostNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "MilePostNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subdivision");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Subdivision"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("railroad");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Railroad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Direction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("velocity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Velocity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("geoFence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GeoFence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GeoFenceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuelVolume");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "FuelVolume"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("throttlePosition");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ThrottlePosition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineRunningState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EngineRunningState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tractionMotorCurrent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TractionMotorCurrent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tractionPower");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TractionPower"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BP_Pressure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "BP_Pressure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BC_Pressure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "BC_Pressure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EOT_BP_Pressure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EOT_BP_Pressure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
