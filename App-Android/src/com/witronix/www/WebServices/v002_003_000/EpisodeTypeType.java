/**
 * EpisodeTypeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class EpisodeTypeType  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedByte episodeTypeCode;

    private java.lang.String episodeName;

    private java.lang.String companyName;

    private java.lang.String episodeDescription;

    private java.lang.String startWiPU_SW_Revision;

    private java.lang.String endWiPU_SW_Revision;

    private java.util.Calendar lastUpdated;

    public EpisodeTypeType() {
    }

    public EpisodeTypeType(
           org.apache.axis.types.UnsignedByte episodeTypeCode,
           java.lang.String episodeName,
           java.lang.String companyName,
           java.lang.String episodeDescription,
           java.lang.String startWiPU_SW_Revision,
           java.lang.String endWiPU_SW_Revision,
           java.util.Calendar lastUpdated) {
           this.episodeTypeCode = episodeTypeCode;
           this.episodeName = episodeName;
           this.companyName = companyName;
           this.episodeDescription = episodeDescription;
           this.startWiPU_SW_Revision = startWiPU_SW_Revision;
           this.endWiPU_SW_Revision = endWiPU_SW_Revision;
           this.lastUpdated = lastUpdated;
    }


    /**
     * Gets the episodeTypeCode value for this EpisodeTypeType.
     * 
     * @return episodeTypeCode
     */
    public org.apache.axis.types.UnsignedByte getEpisodeTypeCode() {
        return episodeTypeCode;
    }


    /**
     * Sets the episodeTypeCode value for this EpisodeTypeType.
     * 
     * @param episodeTypeCode
     */
    public void setEpisodeTypeCode(org.apache.axis.types.UnsignedByte episodeTypeCode) {
        this.episodeTypeCode = episodeTypeCode;
    }


    /**
     * Gets the episodeName value for this EpisodeTypeType.
     * 
     * @return episodeName
     */
    public java.lang.String getEpisodeName() {
        return episodeName;
    }


    /**
     * Sets the episodeName value for this EpisodeTypeType.
     * 
     * @param episodeName
     */
    public void setEpisodeName(java.lang.String episodeName) {
        this.episodeName = episodeName;
    }


    /**
     * Gets the companyName value for this EpisodeTypeType.
     * 
     * @return companyName
     */
    public java.lang.String getCompanyName() {
        return companyName;
    }


    /**
     * Sets the companyName value for this EpisodeTypeType.
     * 
     * @param companyName
     */
    public void setCompanyName(java.lang.String companyName) {
        this.companyName = companyName;
    }


    /**
     * Gets the episodeDescription value for this EpisodeTypeType.
     * 
     * @return episodeDescription
     */
    public java.lang.String getEpisodeDescription() {
        return episodeDescription;
    }


    /**
     * Sets the episodeDescription value for this EpisodeTypeType.
     * 
     * @param episodeDescription
     */
    public void setEpisodeDescription(java.lang.String episodeDescription) {
        this.episodeDescription = episodeDescription;
    }


    /**
     * Gets the startWiPU_SW_Revision value for this EpisodeTypeType.
     * 
     * @return startWiPU_SW_Revision
     */
    public java.lang.String getStartWiPU_SW_Revision() {
        return startWiPU_SW_Revision;
    }


    /**
     * Sets the startWiPU_SW_Revision value for this EpisodeTypeType.
     * 
     * @param startWiPU_SW_Revision
     */
    public void setStartWiPU_SW_Revision(java.lang.String startWiPU_SW_Revision) {
        this.startWiPU_SW_Revision = startWiPU_SW_Revision;
    }


    /**
     * Gets the endWiPU_SW_Revision value for this EpisodeTypeType.
     * 
     * @return endWiPU_SW_Revision
     */
    public java.lang.String getEndWiPU_SW_Revision() {
        return endWiPU_SW_Revision;
    }


    /**
     * Sets the endWiPU_SW_Revision value for this EpisodeTypeType.
     * 
     * @param endWiPU_SW_Revision
     */
    public void setEndWiPU_SW_Revision(java.lang.String endWiPU_SW_Revision) {
        this.endWiPU_SW_Revision = endWiPU_SW_Revision;
    }


    /**
     * Gets the lastUpdated value for this EpisodeTypeType.
     * 
     * @return lastUpdated
     */
    public java.util.Calendar getLastUpdated() {
        return lastUpdated;
    }


    /**
     * Sets the lastUpdated value for this EpisodeTypeType.
     * 
     * @param lastUpdated
     */
    public void setLastUpdated(java.util.Calendar lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EpisodeTypeType)) return false;
        EpisodeTypeType other = (EpisodeTypeType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.episodeTypeCode==null && other.getEpisodeTypeCode()==null) || 
             (this.episodeTypeCode!=null &&
              this.episodeTypeCode.equals(other.getEpisodeTypeCode()))) &&
            ((this.episodeName==null && other.getEpisodeName()==null) || 
             (this.episodeName!=null &&
              this.episodeName.equals(other.getEpisodeName()))) &&
            ((this.companyName==null && other.getCompanyName()==null) || 
             (this.companyName!=null &&
              this.companyName.equals(other.getCompanyName()))) &&
            ((this.episodeDescription==null && other.getEpisodeDescription()==null) || 
             (this.episodeDescription!=null &&
              this.episodeDescription.equals(other.getEpisodeDescription()))) &&
            ((this.startWiPU_SW_Revision==null && other.getStartWiPU_SW_Revision()==null) || 
             (this.startWiPU_SW_Revision!=null &&
              this.startWiPU_SW_Revision.equals(other.getStartWiPU_SW_Revision()))) &&
            ((this.endWiPU_SW_Revision==null && other.getEndWiPU_SW_Revision()==null) || 
             (this.endWiPU_SW_Revision!=null &&
              this.endWiPU_SW_Revision.equals(other.getEndWiPU_SW_Revision()))) &&
            ((this.lastUpdated==null && other.getLastUpdated()==null) || 
             (this.lastUpdated!=null &&
              this.lastUpdated.equals(other.getLastUpdated())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEpisodeTypeCode() != null) {
            _hashCode += getEpisodeTypeCode().hashCode();
        }
        if (getEpisodeName() != null) {
            _hashCode += getEpisodeName().hashCode();
        }
        if (getCompanyName() != null) {
            _hashCode += getCompanyName().hashCode();
        }
        if (getEpisodeDescription() != null) {
            _hashCode += getEpisodeDescription().hashCode();
        }
        if (getStartWiPU_SW_Revision() != null) {
            _hashCode += getStartWiPU_SW_Revision().hashCode();
        }
        if (getEndWiPU_SW_Revision() != null) {
            _hashCode += getEndWiPU_SW_Revision().hashCode();
        }
        if (getLastUpdated() != null) {
            _hashCode += getLastUpdated().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EpisodeTypeType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeTypeType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("episodeTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("episodeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "CompanyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("episodeDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startWiPU_SW_Revision");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "StartWiPU_SW_Revision"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endWiPU_SW_Revision");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EndWiPU_SW_Revision"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdated");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LastUpdated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
