/**
 * RefuelType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class RefuelType  implements java.io.Serializable {
    private java.util.Calendar startOfRefuel;

    private java.util.Calendar endOfRefuel;

    private java.lang.Double latitude;

    private java.lang.Double longitude;

    private java.lang.String location;

    private java.lang.String milePost;

    private java.math.BigDecimal milePostNumber;

    private java.lang.String subdivision;

    private java.lang.String railroad;

    private java.lang.String fenceCode;

    private java.lang.Float fuelVolume;

    private java.lang.Float refuelVolume;

    private java.lang.String trainSymbol;

    private java.util.Calendar dateOfOrigin;

    private java.lang.String trainCategory;

    public RefuelType() {
    }

    public RefuelType(
           java.util.Calendar startOfRefuel,
           java.util.Calendar endOfRefuel,
           java.lang.Double latitude,
           java.lang.Double longitude,
           java.lang.String location,
           java.lang.String milePost,
           java.math.BigDecimal milePostNumber,
           java.lang.String subdivision,
           java.lang.String railroad,
           java.lang.String fenceCode,
           java.lang.Float fuelVolume,
           java.lang.Float refuelVolume,
           java.lang.String trainSymbol,
           java.util.Calendar dateOfOrigin,
           java.lang.String trainCategory) {
           this.startOfRefuel = startOfRefuel;
           this.endOfRefuel = endOfRefuel;
           this.latitude = latitude;
           this.longitude = longitude;
           this.location = location;
           this.milePost = milePost;
           this.milePostNumber = milePostNumber;
           this.subdivision = subdivision;
           this.railroad = railroad;
           this.fenceCode = fenceCode;
           this.fuelVolume = fuelVolume;
           this.refuelVolume = refuelVolume;
           this.trainSymbol = trainSymbol;
           this.dateOfOrigin = dateOfOrigin;
           this.trainCategory = trainCategory;
    }


    /**
     * Gets the startOfRefuel value for this RefuelType.
     * 
     * @return startOfRefuel
     */
    public java.util.Calendar getStartOfRefuel() {
        return startOfRefuel;
    }


    /**
     * Sets the startOfRefuel value for this RefuelType.
     * 
     * @param startOfRefuel
     */
    public void setStartOfRefuel(java.util.Calendar startOfRefuel) {
        this.startOfRefuel = startOfRefuel;
    }


    /**
     * Gets the endOfRefuel value for this RefuelType.
     * 
     * @return endOfRefuel
     */
    public java.util.Calendar getEndOfRefuel() {
        return endOfRefuel;
    }


    /**
     * Sets the endOfRefuel value for this RefuelType.
     * 
     * @param endOfRefuel
     */
    public void setEndOfRefuel(java.util.Calendar endOfRefuel) {
        this.endOfRefuel = endOfRefuel;
    }


    /**
     * Gets the latitude value for this RefuelType.
     * 
     * @return latitude
     */
    public java.lang.Double getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this RefuelType.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.Double latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this RefuelType.
     * 
     * @return longitude
     */
    public java.lang.Double getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this RefuelType.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.Double longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the location value for this RefuelType.
     * 
     * @return location
     */
    public java.lang.String getLocation() {
        return location;
    }


    /**
     * Sets the location value for this RefuelType.
     * 
     * @param location
     */
    public void setLocation(java.lang.String location) {
        this.location = location;
    }


    /**
     * Gets the milePost value for this RefuelType.
     * 
     * @return milePost
     */
    public java.lang.String getMilePost() {
        return milePost;
    }


    /**
     * Sets the milePost value for this RefuelType.
     * 
     * @param milePost
     */
    public void setMilePost(java.lang.String milePost) {
        this.milePost = milePost;
    }


    /**
     * Gets the milePostNumber value for this RefuelType.
     * 
     * @return milePostNumber
     */
    public java.math.BigDecimal getMilePostNumber() {
        return milePostNumber;
    }


    /**
     * Sets the milePostNumber value for this RefuelType.
     * 
     * @param milePostNumber
     */
    public void setMilePostNumber(java.math.BigDecimal milePostNumber) {
        this.milePostNumber = milePostNumber;
    }


    /**
     * Gets the subdivision value for this RefuelType.
     * 
     * @return subdivision
     */
    public java.lang.String getSubdivision() {
        return subdivision;
    }


    /**
     * Sets the subdivision value for this RefuelType.
     * 
     * @param subdivision
     */
    public void setSubdivision(java.lang.String subdivision) {
        this.subdivision = subdivision;
    }


    /**
     * Gets the railroad value for this RefuelType.
     * 
     * @return railroad
     */
    public java.lang.String getRailroad() {
        return railroad;
    }


    /**
     * Sets the railroad value for this RefuelType.
     * 
     * @param railroad
     */
    public void setRailroad(java.lang.String railroad) {
        this.railroad = railroad;
    }


    /**
     * Gets the fenceCode value for this RefuelType.
     * 
     * @return fenceCode
     */
    public java.lang.String getFenceCode() {
        return fenceCode;
    }


    /**
     * Sets the fenceCode value for this RefuelType.
     * 
     * @param fenceCode
     */
    public void setFenceCode(java.lang.String fenceCode) {
        this.fenceCode = fenceCode;
    }


    /**
     * Gets the fuelVolume value for this RefuelType.
     * 
     * @return fuelVolume
     */
    public java.lang.Float getFuelVolume() {
        return fuelVolume;
    }


    /**
     * Sets the fuelVolume value for this RefuelType.
     * 
     * @param fuelVolume
     */
    public void setFuelVolume(java.lang.Float fuelVolume) {
        this.fuelVolume = fuelVolume;
    }


    /**
     * Gets the refuelVolume value for this RefuelType.
     * 
     * @return refuelVolume
     */
    public java.lang.Float getRefuelVolume() {
        return refuelVolume;
    }


    /**
     * Sets the refuelVolume value for this RefuelType.
     * 
     * @param refuelVolume
     */
    public void setRefuelVolume(java.lang.Float refuelVolume) {
        this.refuelVolume = refuelVolume;
    }


    /**
     * Gets the trainSymbol value for this RefuelType.
     * 
     * @return trainSymbol
     */
    public java.lang.String getTrainSymbol() {
        return trainSymbol;
    }


    /**
     * Sets the trainSymbol value for this RefuelType.
     * 
     * @param trainSymbol
     */
    public void setTrainSymbol(java.lang.String trainSymbol) {
        this.trainSymbol = trainSymbol;
    }


    /**
     * Gets the dateOfOrigin value for this RefuelType.
     * 
     * @return dateOfOrigin
     */
    public java.util.Calendar getDateOfOrigin() {
        return dateOfOrigin;
    }


    /**
     * Sets the dateOfOrigin value for this RefuelType.
     * 
     * @param dateOfOrigin
     */
    public void setDateOfOrigin(java.util.Calendar dateOfOrigin) {
        this.dateOfOrigin = dateOfOrigin;
    }


    /**
     * Gets the trainCategory value for this RefuelType.
     * 
     * @return trainCategory
     */
    public java.lang.String getTrainCategory() {
        return trainCategory;
    }


    /**
     * Sets the trainCategory value for this RefuelType.
     * 
     * @param trainCategory
     */
    public void setTrainCategory(java.lang.String trainCategory) {
        this.trainCategory = trainCategory;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RefuelType)) return false;
        RefuelType other = (RefuelType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.startOfRefuel==null && other.getStartOfRefuel()==null) || 
             (this.startOfRefuel!=null &&
              this.startOfRefuel.equals(other.getStartOfRefuel()))) &&
            ((this.endOfRefuel==null && other.getEndOfRefuel()==null) || 
             (this.endOfRefuel!=null &&
              this.endOfRefuel.equals(other.getEndOfRefuel()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation()))) &&
            ((this.milePost==null && other.getMilePost()==null) || 
             (this.milePost!=null &&
              this.milePost.equals(other.getMilePost()))) &&
            ((this.milePostNumber==null && other.getMilePostNumber()==null) || 
             (this.milePostNumber!=null &&
              this.milePostNumber.equals(other.getMilePostNumber()))) &&
            ((this.subdivision==null && other.getSubdivision()==null) || 
             (this.subdivision!=null &&
              this.subdivision.equals(other.getSubdivision()))) &&
            ((this.railroad==null && other.getRailroad()==null) || 
             (this.railroad!=null &&
              this.railroad.equals(other.getRailroad()))) &&
            ((this.fenceCode==null && other.getFenceCode()==null) || 
             (this.fenceCode!=null &&
              this.fenceCode.equals(other.getFenceCode()))) &&
            ((this.fuelVolume==null && other.getFuelVolume()==null) || 
             (this.fuelVolume!=null &&
              this.fuelVolume.equals(other.getFuelVolume()))) &&
            ((this.refuelVolume==null && other.getRefuelVolume()==null) || 
             (this.refuelVolume!=null &&
              this.refuelVolume.equals(other.getRefuelVolume()))) &&
            ((this.trainSymbol==null && other.getTrainSymbol()==null) || 
             (this.trainSymbol!=null &&
              this.trainSymbol.equals(other.getTrainSymbol()))) &&
            ((this.dateOfOrigin==null && other.getDateOfOrigin()==null) || 
             (this.dateOfOrigin!=null &&
              this.dateOfOrigin.equals(other.getDateOfOrigin()))) &&
            ((this.trainCategory==null && other.getTrainCategory()==null) || 
             (this.trainCategory!=null &&
              this.trainCategory.equals(other.getTrainCategory())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStartOfRefuel() != null) {
            _hashCode += getStartOfRefuel().hashCode();
        }
        if (getEndOfRefuel() != null) {
            _hashCode += getEndOfRefuel().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        if (getMilePost() != null) {
            _hashCode += getMilePost().hashCode();
        }
        if (getMilePostNumber() != null) {
            _hashCode += getMilePostNumber().hashCode();
        }
        if (getSubdivision() != null) {
            _hashCode += getSubdivision().hashCode();
        }
        if (getRailroad() != null) {
            _hashCode += getRailroad().hashCode();
        }
        if (getFenceCode() != null) {
            _hashCode += getFenceCode().hashCode();
        }
        if (getFuelVolume() != null) {
            _hashCode += getFuelVolume().hashCode();
        }
        if (getRefuelVolume() != null) {
            _hashCode += getRefuelVolume().hashCode();
        }
        if (getTrainSymbol() != null) {
            _hashCode += getTrainSymbol().hashCode();
        }
        if (getDateOfOrigin() != null) {
            _hashCode += getDateOfOrigin().hashCode();
        }
        if (getTrainCategory() != null) {
            _hashCode += getTrainCategory().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RefuelType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "RefuelType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startOfRefuel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "StartOfRefuel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endOfRefuel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EndOfRefuel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Location"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("milePost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "MilePost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("milePostNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "MilePostNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subdivision");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Subdivision"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("railroad");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Railroad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fenceCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "FenceCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuelVolume");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "FuelVolume"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refuelVolume");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "RefuelVolume"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainSymbol");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TrainSymbol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "DateOfOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TrainCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
