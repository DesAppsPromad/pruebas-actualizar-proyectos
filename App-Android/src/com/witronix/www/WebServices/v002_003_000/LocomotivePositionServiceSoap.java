/**
 * LocomotivePositionServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public interface LocomotivePositionServiceSoap extends java.rmi.Remote {
    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotiveLatestPositionByUnitNumber(java.lang.String username, java.lang.String password, java.lang.String initials, java.lang.String unitNumber) throws java.rmi.RemoteException;
    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotiveLatestPosition(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesPositionsByRecordtime(java.lang.String username, java.lang.String password, java.util.Calendar recordTime) throws java.rmi.RemoteException;
    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesPositionsByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime) throws java.rmi.RemoteException;
    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getLocomotivesRapidSamplesByRecordtime(java.lang.String username, java.lang.String password, java.lang.String recordStartTime, java.lang.String recordEndTime) throws java.rmi.RemoteException;
    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getRefuelsByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime) throws java.rmi.RemoteException;
    public com.witronix.www.WebServices.v002_003_000.LocomotivePositionRS getEpisodesByDateRange(java.lang.String username, java.lang.String password, java.util.Calendar recordStartTime, java.util.Calendar recordEndTime, java.lang.String episodeTypes) throws java.rmi.RemoteException;
    public com.witronix.www.WebServices.v002_003_000.InfoRS getEpisodeTypes(java.lang.String username, java.lang.String password, boolean entireHistory, int languageID) throws java.rmi.RemoteException;
}
