/**
 * EpisodeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class EpisodeType  implements java.io.Serializable {
    private org.apache.axis.types.UnsignedByte episodeTypeCode;

    private java.util.Calendar startTime;

    private java.lang.Float duration;

    private java.lang.String trainSymbol;

    private java.util.Calendar dateOfOrigin;

    private java.lang.String trainCategory;

    private java.lang.Float speedLimit;

    private com.witronix.www.WebServices.v002_003_000.EpisodeDataType episodeDataAtStart;

    private com.witronix.www.WebServices.v002_003_000.EpisodeDataType episodeDataAtEnd;

    public EpisodeType() {
    }

    public EpisodeType(
           org.apache.axis.types.UnsignedByte episodeTypeCode,
           java.util.Calendar startTime,
           java.lang.Float duration,
           java.lang.String trainSymbol,
           java.util.Calendar dateOfOrigin,
           java.lang.String trainCategory,
           java.lang.Float speedLimit,
           com.witronix.www.WebServices.v002_003_000.EpisodeDataType episodeDataAtStart,
           com.witronix.www.WebServices.v002_003_000.EpisodeDataType episodeDataAtEnd) {
           this.episodeTypeCode = episodeTypeCode;
           this.startTime = startTime;
           this.duration = duration;
           this.trainSymbol = trainSymbol;
           this.dateOfOrigin = dateOfOrigin;
           this.trainCategory = trainCategory;
           this.speedLimit = speedLimit;
           this.episodeDataAtStart = episodeDataAtStart;
           this.episodeDataAtEnd = episodeDataAtEnd;
    }


    /**
     * Gets the episodeTypeCode value for this EpisodeType.
     * 
     * @return episodeTypeCode
     */
    public org.apache.axis.types.UnsignedByte getEpisodeTypeCode() {
        return episodeTypeCode;
    }


    /**
     * Sets the episodeTypeCode value for this EpisodeType.
     * 
     * @param episodeTypeCode
     */
    public void setEpisodeTypeCode(org.apache.axis.types.UnsignedByte episodeTypeCode) {
        this.episodeTypeCode = episodeTypeCode;
    }


    /**
     * Gets the startTime value for this EpisodeType.
     * 
     * @return startTime
     */
    public java.util.Calendar getStartTime() {
        return startTime;
    }


    /**
     * Sets the startTime value for this EpisodeType.
     * 
     * @param startTime
     */
    public void setStartTime(java.util.Calendar startTime) {
        this.startTime = startTime;
    }


    /**
     * Gets the duration value for this EpisodeType.
     * 
     * @return duration
     */
    public java.lang.Float getDuration() {
        return duration;
    }


    /**
     * Sets the duration value for this EpisodeType.
     * 
     * @param duration
     */
    public void setDuration(java.lang.Float duration) {
        this.duration = duration;
    }


    /**
     * Gets the trainSymbol value for this EpisodeType.
     * 
     * @return trainSymbol
     */
    public java.lang.String getTrainSymbol() {
        return trainSymbol;
    }


    /**
     * Sets the trainSymbol value for this EpisodeType.
     * 
     * @param trainSymbol
     */
    public void setTrainSymbol(java.lang.String trainSymbol) {
        this.trainSymbol = trainSymbol;
    }


    /**
     * Gets the dateOfOrigin value for this EpisodeType.
     * 
     * @return dateOfOrigin
     */
    public java.util.Calendar getDateOfOrigin() {
        return dateOfOrigin;
    }


    /**
     * Sets the dateOfOrigin value for this EpisodeType.
     * 
     * @param dateOfOrigin
     */
    public void setDateOfOrigin(java.util.Calendar dateOfOrigin) {
        this.dateOfOrigin = dateOfOrigin;
    }


    /**
     * Gets the trainCategory value for this EpisodeType.
     * 
     * @return trainCategory
     */
    public java.lang.String getTrainCategory() {
        return trainCategory;
    }


    /**
     * Sets the trainCategory value for this EpisodeType.
     * 
     * @param trainCategory
     */
    public void setTrainCategory(java.lang.String trainCategory) {
        this.trainCategory = trainCategory;
    }


    /**
     * Gets the speedLimit value for this EpisodeType.
     * 
     * @return speedLimit
     */
    public java.lang.Float getSpeedLimit() {
        return speedLimit;
    }


    /**
     * Sets the speedLimit value for this EpisodeType.
     * 
     * @param speedLimit
     */
    public void setSpeedLimit(java.lang.Float speedLimit) {
        this.speedLimit = speedLimit;
    }


    /**
     * Gets the episodeDataAtStart value for this EpisodeType.
     * 
     * @return episodeDataAtStart
     */
    public com.witronix.www.WebServices.v002_003_000.EpisodeDataType getEpisodeDataAtStart() {
        return episodeDataAtStart;
    }


    /**
     * Sets the episodeDataAtStart value for this EpisodeType.
     * 
     * @param episodeDataAtStart
     */
    public void setEpisodeDataAtStart(com.witronix.www.WebServices.v002_003_000.EpisodeDataType episodeDataAtStart) {
        this.episodeDataAtStart = episodeDataAtStart;
    }


    /**
     * Gets the episodeDataAtEnd value for this EpisodeType.
     * 
     * @return episodeDataAtEnd
     */
    public com.witronix.www.WebServices.v002_003_000.EpisodeDataType getEpisodeDataAtEnd() {
        return episodeDataAtEnd;
    }


    /**
     * Sets the episodeDataAtEnd value for this EpisodeType.
     * 
     * @param episodeDataAtEnd
     */
    public void setEpisodeDataAtEnd(com.witronix.www.WebServices.v002_003_000.EpisodeDataType episodeDataAtEnd) {
        this.episodeDataAtEnd = episodeDataAtEnd;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EpisodeType)) return false;
        EpisodeType other = (EpisodeType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.episodeTypeCode==null && other.getEpisodeTypeCode()==null) || 
             (this.episodeTypeCode!=null &&
              this.episodeTypeCode.equals(other.getEpisodeTypeCode()))) &&
            ((this.startTime==null && other.getStartTime()==null) || 
             (this.startTime!=null &&
              this.startTime.equals(other.getStartTime()))) &&
            ((this.duration==null && other.getDuration()==null) || 
             (this.duration!=null &&
              this.duration.equals(other.getDuration()))) &&
            ((this.trainSymbol==null && other.getTrainSymbol()==null) || 
             (this.trainSymbol!=null &&
              this.trainSymbol.equals(other.getTrainSymbol()))) &&
            ((this.dateOfOrigin==null && other.getDateOfOrigin()==null) || 
             (this.dateOfOrigin!=null &&
              this.dateOfOrigin.equals(other.getDateOfOrigin()))) &&
            ((this.trainCategory==null && other.getTrainCategory()==null) || 
             (this.trainCategory!=null &&
              this.trainCategory.equals(other.getTrainCategory()))) &&
            ((this.speedLimit==null && other.getSpeedLimit()==null) || 
             (this.speedLimit!=null &&
              this.speedLimit.equals(other.getSpeedLimit()))) &&
            ((this.episodeDataAtStart==null && other.getEpisodeDataAtStart()==null) || 
             (this.episodeDataAtStart!=null &&
              this.episodeDataAtStart.equals(other.getEpisodeDataAtStart()))) &&
            ((this.episodeDataAtEnd==null && other.getEpisodeDataAtEnd()==null) || 
             (this.episodeDataAtEnd!=null &&
              this.episodeDataAtEnd.equals(other.getEpisodeDataAtEnd())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEpisodeTypeCode() != null) {
            _hashCode += getEpisodeTypeCode().hashCode();
        }
        if (getStartTime() != null) {
            _hashCode += getStartTime().hashCode();
        }
        if (getDuration() != null) {
            _hashCode += getDuration().hashCode();
        }
        if (getTrainSymbol() != null) {
            _hashCode += getTrainSymbol().hashCode();
        }
        if (getDateOfOrigin() != null) {
            _hashCode += getDateOfOrigin().hashCode();
        }
        if (getTrainCategory() != null) {
            _hashCode += getTrainCategory().hashCode();
        }
        if (getSpeedLimit() != null) {
            _hashCode += getSpeedLimit().hashCode();
        }
        if (getEpisodeDataAtStart() != null) {
            _hashCode += getEpisodeDataAtStart().hashCode();
        }
        if (getEpisodeDataAtEnd() != null) {
            _hashCode += getEpisodeDataAtEnd().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EpisodeType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("episodeTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedByte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "StartTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Duration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainSymbol");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TrainSymbol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "DateOfOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TrainCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("speedLimit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "SpeedLimit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("episodeDataAtStart");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeDataAtStart"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("episodeDataAtEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeDataAtEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
