/**
 * PositionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class PositionType  implements java.io.Serializable {
    private java.lang.String trainSymbol;

    private java.lang.String dateOfOrigin;

    private java.lang.String trainCategory;

    private java.lang.String engineRunningState;

    private java.lang.String HEP_Mode;

    private java.lang.String altitude;

    private java.lang.String temperature;

    private java.lang.String throttlePosition;

    private java.lang.String tractionMotorCurrent;

    private java.lang.String BP_Pres;

    private java.lang.String BC_Pres;

    private java.lang.String DVR_HealthStatus;

    private java.lang.String tractionPower;

    private java.util.Calendar positionTime;

    private java.math.BigDecimal latitude;

    private java.math.BigDecimal longitude;

    private java.lang.String location;

    private java.lang.String milepost;

    private java.math.BigDecimal direction;

    private java.math.BigDecimal velocity;

    private java.math.BigDecimal fuelVolume;

    private com.witronix.www.WebServices.v002_003_000.GeoFenceType geoFence;

    public PositionType() {
    }

    public PositionType(
           java.lang.String trainSymbol,
           java.lang.String dateOfOrigin,
           java.lang.String trainCategory,
           java.lang.String engineRunningState,
           java.lang.String HEP_Mode,
           java.lang.String altitude,
           java.lang.String temperature,
           java.lang.String throttlePosition,
           java.lang.String tractionMotorCurrent,
           java.lang.String BP_Pres,
           java.lang.String BC_Pres,
           java.lang.String DVR_HealthStatus,
           java.lang.String tractionPower,
           java.util.Calendar positionTime,
           java.math.BigDecimal latitude,
           java.math.BigDecimal longitude,
           java.lang.String location,
           java.lang.String milepost,
           java.math.BigDecimal direction,
           java.math.BigDecimal velocity,
           java.math.BigDecimal fuelVolume,
           com.witronix.www.WebServices.v002_003_000.GeoFenceType geoFence) {
           this.trainSymbol = trainSymbol;
           this.dateOfOrigin = dateOfOrigin;
           this.trainCategory = trainCategory;
           this.engineRunningState = engineRunningState;
           this.HEP_Mode = HEP_Mode;
           this.altitude = altitude;
           this.temperature = temperature;
           this.throttlePosition = throttlePosition;
           this.tractionMotorCurrent = tractionMotorCurrent;
           this.BP_Pres = BP_Pres;
           this.BC_Pres = BC_Pres;
           this.DVR_HealthStatus = DVR_HealthStatus;
           this.tractionPower = tractionPower;
           this.positionTime = positionTime;
           this.latitude = latitude;
           this.longitude = longitude;
           this.location = location;
           this.milepost = milepost;
           this.direction = direction;
           this.velocity = velocity;
           this.fuelVolume = fuelVolume;
           this.geoFence = geoFence;
    }


    /**
     * Gets the trainSymbol value for this PositionType.
     * 
     * @return trainSymbol
     */
    public java.lang.String getTrainSymbol() {
        return trainSymbol;
    }


    /**
     * Sets the trainSymbol value for this PositionType.
     * 
     * @param trainSymbol
     */
    public void setTrainSymbol(java.lang.String trainSymbol) {
        this.trainSymbol = trainSymbol;
    }


    /**
     * Gets the dateOfOrigin value for this PositionType.
     * 
     * @return dateOfOrigin
     */
    public java.lang.String getDateOfOrigin() {
        return dateOfOrigin;
    }


    /**
     * Sets the dateOfOrigin value for this PositionType.
     * 
     * @param dateOfOrigin
     */
    public void setDateOfOrigin(java.lang.String dateOfOrigin) {
        this.dateOfOrigin = dateOfOrigin;
    }


    /**
     * Gets the trainCategory value for this PositionType.
     * 
     * @return trainCategory
     */
    public java.lang.String getTrainCategory() {
        return trainCategory;
    }


    /**
     * Sets the trainCategory value for this PositionType.
     * 
     * @param trainCategory
     */
    public void setTrainCategory(java.lang.String trainCategory) {
        this.trainCategory = trainCategory;
    }


    /**
     * Gets the engineRunningState value for this PositionType.
     * 
     * @return engineRunningState
     */
    public java.lang.String getEngineRunningState() {
        return engineRunningState;
    }


    /**
     * Sets the engineRunningState value for this PositionType.
     * 
     * @param engineRunningState
     */
    public void setEngineRunningState(java.lang.String engineRunningState) {
        this.engineRunningState = engineRunningState;
    }


    /**
     * Gets the HEP_Mode value for this PositionType.
     * 
     * @return HEP_Mode
     */
    public java.lang.String getHEP_Mode() {
        return HEP_Mode;
    }


    /**
     * Sets the HEP_Mode value for this PositionType.
     * 
     * @param HEP_Mode
     */
    public void setHEP_Mode(java.lang.String HEP_Mode) {
        this.HEP_Mode = HEP_Mode;
    }


    /**
     * Gets the altitude value for this PositionType.
     * 
     * @return altitude
     */
    public java.lang.String getAltitude() {
        return altitude;
    }


    /**
     * Sets the altitude value for this PositionType.
     * 
     * @param altitude
     */
    public void setAltitude(java.lang.String altitude) {
        this.altitude = altitude;
    }


    /**
     * Gets the temperature value for this PositionType.
     * 
     * @return temperature
     */
    public java.lang.String getTemperature() {
        return temperature;
    }


    /**
     * Sets the temperature value for this PositionType.
     * 
     * @param temperature
     */
    public void setTemperature(java.lang.String temperature) {
        this.temperature = temperature;
    }


    /**
     * Gets the throttlePosition value for this PositionType.
     * 
     * @return throttlePosition
     */
    public java.lang.String getThrottlePosition() {
        return throttlePosition;
    }


    /**
     * Sets the throttlePosition value for this PositionType.
     * 
     * @param throttlePosition
     */
    public void setThrottlePosition(java.lang.String throttlePosition) {
        this.throttlePosition = throttlePosition;
    }


    /**
     * Gets the tractionMotorCurrent value for this PositionType.
     * 
     * @return tractionMotorCurrent
     */
    public java.lang.String getTractionMotorCurrent() {
        return tractionMotorCurrent;
    }


    /**
     * Sets the tractionMotorCurrent value for this PositionType.
     * 
     * @param tractionMotorCurrent
     */
    public void setTractionMotorCurrent(java.lang.String tractionMotorCurrent) {
        this.tractionMotorCurrent = tractionMotorCurrent;
    }


    /**
     * Gets the BP_Pres value for this PositionType.
     * 
     * @return BP_Pres
     */
    public java.lang.String getBP_Pres() {
        return BP_Pres;
    }


    /**
     * Sets the BP_Pres value for this PositionType.
     * 
     * @param BP_Pres
     */
    public void setBP_Pres(java.lang.String BP_Pres) {
        this.BP_Pres = BP_Pres;
    }


    /**
     * Gets the BC_Pres value for this PositionType.
     * 
     * @return BC_Pres
     */
    public java.lang.String getBC_Pres() {
        return BC_Pres;
    }


    /**
     * Sets the BC_Pres value for this PositionType.
     * 
     * @param BC_Pres
     */
    public void setBC_Pres(java.lang.String BC_Pres) {
        this.BC_Pres = BC_Pres;
    }


    /**
     * Gets the DVR_HealthStatus value for this PositionType.
     * 
     * @return DVR_HealthStatus
     */
    public java.lang.String getDVR_HealthStatus() {
        return DVR_HealthStatus;
    }


    /**
     * Sets the DVR_HealthStatus value for this PositionType.
     * 
     * @param DVR_HealthStatus
     */
    public void setDVR_HealthStatus(java.lang.String DVR_HealthStatus) {
        this.DVR_HealthStatus = DVR_HealthStatus;
    }


    /**
     * Gets the tractionPower value for this PositionType.
     * 
     * @return tractionPower
     */
    public java.lang.String getTractionPower() {
        return tractionPower;
    }


    /**
     * Sets the tractionPower value for this PositionType.
     * 
     * @param tractionPower
     */
    public void setTractionPower(java.lang.String tractionPower) {
        this.tractionPower = tractionPower;
    }


    /**
     * Gets the positionTime value for this PositionType.
     * 
     * @return positionTime
     */
    public java.util.Calendar getPositionTime() {
        return positionTime;
    }


    /**
     * Sets the positionTime value for this PositionType.
     * 
     * @param positionTime
     */
    public void setPositionTime(java.util.Calendar positionTime) {
        this.positionTime = positionTime;
    }


    /**
     * Gets the latitude value for this PositionType.
     * 
     * @return latitude
     */
    public java.math.BigDecimal getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this PositionType.
     * 
     * @param latitude
     */
    public void setLatitude(java.math.BigDecimal latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this PositionType.
     * 
     * @return longitude
     */
    public java.math.BigDecimal getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this PositionType.
     * 
     * @param longitude
     */
    public void setLongitude(java.math.BigDecimal longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the location value for this PositionType.
     * 
     * @return location
     */
    public java.lang.String getLocation() {
        return location;
    }


    /**
     * Sets the location value for this PositionType.
     * 
     * @param location
     */
    public void setLocation(java.lang.String location) {
        this.location = location;
    }


    /**
     * Gets the milepost value for this PositionType.
     * 
     * @return milepost
     */
    public java.lang.String getMilepost() {
        return milepost;
    }


    /**
     * Sets the milepost value for this PositionType.
     * 
     * @param milepost
     */
    public void setMilepost(java.lang.String milepost) {
        this.milepost = milepost;
    }


    /**
     * Gets the direction value for this PositionType.
     * 
     * @return direction
     */
    public java.math.BigDecimal getDirection() {
        return direction;
    }


    /**
     * Sets the direction value for this PositionType.
     * 
     * @param direction
     */
    public void setDirection(java.math.BigDecimal direction) {
        this.direction = direction;
    }


    /**
     * Gets the velocity value for this PositionType.
     * 
     * @return velocity
     */
    public java.math.BigDecimal getVelocity() {
        return velocity;
    }


    /**
     * Sets the velocity value for this PositionType.
     * 
     * @param velocity
     */
    public void setVelocity(java.math.BigDecimal velocity) {
        this.velocity = velocity;
    }


    /**
     * Gets the fuelVolume value for this PositionType.
     * 
     * @return fuelVolume
     */
    public java.math.BigDecimal getFuelVolume() {
        return fuelVolume;
    }


    /**
     * Sets the fuelVolume value for this PositionType.
     * 
     * @param fuelVolume
     */
    public void setFuelVolume(java.math.BigDecimal fuelVolume) {
        this.fuelVolume = fuelVolume;
    }


    /**
     * Gets the geoFence value for this PositionType.
     * 
     * @return geoFence
     */
    public com.witronix.www.WebServices.v002_003_000.GeoFenceType getGeoFence() {
        return geoFence;
    }


    /**
     * Sets the geoFence value for this PositionType.
     * 
     * @param geoFence
     */
    public void setGeoFence(com.witronix.www.WebServices.v002_003_000.GeoFenceType geoFence) {
        this.geoFence = geoFence;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PositionType)) return false;
        PositionType other = (PositionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.trainSymbol==null && other.getTrainSymbol()==null) || 
             (this.trainSymbol!=null &&
              this.trainSymbol.equals(other.getTrainSymbol()))) &&
            ((this.dateOfOrigin==null && other.getDateOfOrigin()==null) || 
             (this.dateOfOrigin!=null &&
              this.dateOfOrigin.equals(other.getDateOfOrigin()))) &&
            ((this.trainCategory==null && other.getTrainCategory()==null) || 
             (this.trainCategory!=null &&
              this.trainCategory.equals(other.getTrainCategory()))) &&
            ((this.engineRunningState==null && other.getEngineRunningState()==null) || 
             (this.engineRunningState!=null &&
              this.engineRunningState.equals(other.getEngineRunningState()))) &&
            ((this.HEP_Mode==null && other.getHEP_Mode()==null) || 
             (this.HEP_Mode!=null &&
              this.HEP_Mode.equals(other.getHEP_Mode()))) &&
            ((this.altitude==null && other.getAltitude()==null) || 
             (this.altitude!=null &&
              this.altitude.equals(other.getAltitude()))) &&
            ((this.temperature==null && other.getTemperature()==null) || 
             (this.temperature!=null &&
              this.temperature.equals(other.getTemperature()))) &&
            ((this.throttlePosition==null && other.getThrottlePosition()==null) || 
             (this.throttlePosition!=null &&
              this.throttlePosition.equals(other.getThrottlePosition()))) &&
            ((this.tractionMotorCurrent==null && other.getTractionMotorCurrent()==null) || 
             (this.tractionMotorCurrent!=null &&
              this.tractionMotorCurrent.equals(other.getTractionMotorCurrent()))) &&
            ((this.BP_Pres==null && other.getBP_Pres()==null) || 
             (this.BP_Pres!=null &&
              this.BP_Pres.equals(other.getBP_Pres()))) &&
            ((this.BC_Pres==null && other.getBC_Pres()==null) || 
             (this.BC_Pres!=null &&
              this.BC_Pres.equals(other.getBC_Pres()))) &&
            ((this.DVR_HealthStatus==null && other.getDVR_HealthStatus()==null) || 
             (this.DVR_HealthStatus!=null &&
              this.DVR_HealthStatus.equals(other.getDVR_HealthStatus()))) &&
            ((this.tractionPower==null && other.getTractionPower()==null) || 
             (this.tractionPower!=null &&
              this.tractionPower.equals(other.getTractionPower()))) &&
            ((this.positionTime==null && other.getPositionTime()==null) || 
             (this.positionTime!=null &&
              this.positionTime.equals(other.getPositionTime()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation()))) &&
            ((this.milepost==null && other.getMilepost()==null) || 
             (this.milepost!=null &&
              this.milepost.equals(other.getMilepost()))) &&
            ((this.direction==null && other.getDirection()==null) || 
             (this.direction!=null &&
              this.direction.equals(other.getDirection()))) &&
            ((this.velocity==null && other.getVelocity()==null) || 
             (this.velocity!=null &&
              this.velocity.equals(other.getVelocity()))) &&
            ((this.fuelVolume==null && other.getFuelVolume()==null) || 
             (this.fuelVolume!=null &&
              this.fuelVolume.equals(other.getFuelVolume()))) &&
            ((this.geoFence==null && other.getGeoFence()==null) || 
             (this.geoFence!=null &&
              this.geoFence.equals(other.getGeoFence())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTrainSymbol() != null) {
            _hashCode += getTrainSymbol().hashCode();
        }
        if (getDateOfOrigin() != null) {
            _hashCode += getDateOfOrigin().hashCode();
        }
        if (getTrainCategory() != null) {
            _hashCode += getTrainCategory().hashCode();
        }
        if (getEngineRunningState() != null) {
            _hashCode += getEngineRunningState().hashCode();
        }
        if (getHEP_Mode() != null) {
            _hashCode += getHEP_Mode().hashCode();
        }
        if (getAltitude() != null) {
            _hashCode += getAltitude().hashCode();
        }
        if (getTemperature() != null) {
            _hashCode += getTemperature().hashCode();
        }
        if (getThrottlePosition() != null) {
            _hashCode += getThrottlePosition().hashCode();
        }
        if (getTractionMotorCurrent() != null) {
            _hashCode += getTractionMotorCurrent().hashCode();
        }
        if (getBP_Pres() != null) {
            _hashCode += getBP_Pres().hashCode();
        }
        if (getBC_Pres() != null) {
            _hashCode += getBC_Pres().hashCode();
        }
        if (getDVR_HealthStatus() != null) {
            _hashCode += getDVR_HealthStatus().hashCode();
        }
        if (getTractionPower() != null) {
            _hashCode += getTractionPower().hashCode();
        }
        if (getPositionTime() != null) {
            _hashCode += getPositionTime().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        if (getMilepost() != null) {
            _hashCode += getMilepost().hashCode();
        }
        if (getDirection() != null) {
            _hashCode += getDirection().hashCode();
        }
        if (getVelocity() != null) {
            _hashCode += getVelocity().hashCode();
        }
        if (getFuelVolume() != null) {
            _hashCode += getFuelVolume().hashCode();
        }
        if (getGeoFence() != null) {
            _hashCode += getGeoFence().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PositionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "PositionType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainSymbol");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TrainSymbol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "DateOfOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TrainCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineRunningState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EngineRunningState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HEP_Mode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "HEP_Mode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Altitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temperature");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Temperature"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("throttlePosition");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ThrottlePosition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tractionMotorCurrent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TractionMotorCurrent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BP_Pres");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "BP_Pres"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BC_Pres");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "BC_Pres"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DVR_HealthStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "DVR_HealthStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tractionPower");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "TractionPower"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("positionTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "PositionTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Location"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("milepost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Milepost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Direction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("velocity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Velocity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuelVolume");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "FuelVolume"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("geoFence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GeoFence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "GeoFenceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
