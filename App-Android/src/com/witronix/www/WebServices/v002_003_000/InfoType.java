/**
 * InfoType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class InfoType  implements java.io.Serializable {
    private com.witronix.www.WebServices.v002_003_000.ErrorType[] errors;

    private com.witronix.www.WebServices.v002_003_000.EpisodeTypeType[] episodeTypes;

    public InfoType() {
    }

    public InfoType(
           com.witronix.www.WebServices.v002_003_000.ErrorType[] errors,
           com.witronix.www.WebServices.v002_003_000.EpisodeTypeType[] episodeTypes) {
           this.errors = errors;
           this.episodeTypes = episodeTypes;
    }


    /**
     * Gets the errors value for this InfoType.
     * 
     * @return errors
     */
    public com.witronix.www.WebServices.v002_003_000.ErrorType[] getErrors() {
        return errors;
    }


    /**
     * Sets the errors value for this InfoType.
     * 
     * @param errors
     */
    public void setErrors(com.witronix.www.WebServices.v002_003_000.ErrorType[] errors) {
        this.errors = errors;
    }


    /**
     * Gets the episodeTypes value for this InfoType.
     * 
     * @return episodeTypes
     */
    public com.witronix.www.WebServices.v002_003_000.EpisodeTypeType[] getEpisodeTypes() {
        return episodeTypes;
    }


    /**
     * Sets the episodeTypes value for this InfoType.
     * 
     * @param episodeTypes
     */
    public void setEpisodeTypes(com.witronix.www.WebServices.v002_003_000.EpisodeTypeType[] episodeTypes) {
        this.episodeTypes = episodeTypes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InfoType)) return false;
        InfoType other = (InfoType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errors==null && other.getErrors()==null) || 
             (this.errors!=null &&
              java.util.Arrays.equals(this.errors, other.getErrors()))) &&
            ((this.episodeTypes==null && other.getEpisodeTypes()==null) || 
             (this.episodeTypes!=null &&
              java.util.Arrays.equals(this.episodeTypes, other.getEpisodeTypes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrors() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErrors());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErrors(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEpisodeTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEpisodeTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEpisodeTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InfoType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "InfoType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errors");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Errors"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ErrorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Error"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("episodeTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeTypeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
