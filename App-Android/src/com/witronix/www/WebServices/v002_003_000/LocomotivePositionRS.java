/**
 * LocomotivePositionRS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class LocomotivePositionRS  implements java.io.Serializable {
    private com.witronix.www.WebServices.v002_003_000.LocomotiveType[] locomotives;

    public LocomotivePositionRS() {
    }

    public LocomotivePositionRS(
           com.witronix.www.WebServices.v002_003_000.LocomotiveType[] locomotives) {
           this.locomotives = locomotives;
    }


    /**
     * Gets the locomotives value for this LocomotivePositionRS.
     * 
     * @return locomotives
     */
    public com.witronix.www.WebServices.v002_003_000.LocomotiveType[] getLocomotives() {
        return locomotives;
    }


    /**
     * Sets the locomotives value for this LocomotivePositionRS.
     * 
     * @param locomotives
     */
    public void setLocomotives(com.witronix.www.WebServices.v002_003_000.LocomotiveType[] locomotives) {
        this.locomotives = locomotives;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocomotivePositionRS)) return false;
        LocomotivePositionRS other = (LocomotivePositionRS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.locomotives==null && other.getLocomotives()==null) || 
             (this.locomotives!=null &&
              java.util.Arrays.equals(this.locomotives, other.getLocomotives())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLocomotives() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLocomotives());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLocomotives(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocomotivePositionRS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotivePositionRS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locomotives");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Locomotives"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotiveType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Locomotive"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
