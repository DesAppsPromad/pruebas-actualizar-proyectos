/**
 * LocomotiveType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.witronix.www.WebServices.v002_003_000;

public class LocomotiveType  implements java.io.Serializable {
    private com.witronix.www.WebServices.v002_003_000.ErrorType[] errors;

    private com.witronix.www.WebServices.v002_003_000.PositionType[] positions;

    private com.witronix.www.WebServices.v002_003_000.RefuelType[] refuels;

    private com.witronix.www.WebServices.v002_003_000.EpisodeType[] episodes;

    private int unitNumber;  // attribute

    private java.lang.String unitInitial;  // attribute

    public LocomotiveType() {
    }

    public LocomotiveType(
           com.witronix.www.WebServices.v002_003_000.ErrorType[] errors,
           com.witronix.www.WebServices.v002_003_000.PositionType[] positions,
           com.witronix.www.WebServices.v002_003_000.RefuelType[] refuels,
           com.witronix.www.WebServices.v002_003_000.EpisodeType[] episodes,
           int unitNumber,
           java.lang.String unitInitial) {
           this.errors = errors;
           this.positions = positions;
           this.refuels = refuels;
           this.episodes = episodes;
           this.unitNumber = unitNumber;
           this.unitInitial = unitInitial;
    }


    /**
     * Gets the errors value for this LocomotiveType.
     * 
     * @return errors
     */
    public com.witronix.www.WebServices.v002_003_000.ErrorType[] getErrors() {
        return errors;
    }


    /**
     * Sets the errors value for this LocomotiveType.
     * 
     * @param errors
     */
    public void setErrors(com.witronix.www.WebServices.v002_003_000.ErrorType[] errors) {
        this.errors = errors;
    }


    /**
     * Gets the positions value for this LocomotiveType.
     * 
     * @return positions
     */
    public com.witronix.www.WebServices.v002_003_000.PositionType[] getPositions() {
        return positions;
    }


    /**
     * Sets the positions value for this LocomotiveType.
     * 
     * @param positions
     */
    public void setPositions(com.witronix.www.WebServices.v002_003_000.PositionType[] positions) {
        this.positions = positions;
    }


    /**
     * Gets the refuels value for this LocomotiveType.
     * 
     * @return refuels
     */
    public com.witronix.www.WebServices.v002_003_000.RefuelType[] getRefuels() {
        return refuels;
    }


    /**
     * Sets the refuels value for this LocomotiveType.
     * 
     * @param refuels
     */
    public void setRefuels(com.witronix.www.WebServices.v002_003_000.RefuelType[] refuels) {
        this.refuels = refuels;
    }


    /**
     * Gets the episodes value for this LocomotiveType.
     * 
     * @return episodes
     */
    public com.witronix.www.WebServices.v002_003_000.EpisodeType[] getEpisodes() {
        return episodes;
    }


    /**
     * Sets the episodes value for this LocomotiveType.
     * 
     * @param episodes
     */
    public void setEpisodes(com.witronix.www.WebServices.v002_003_000.EpisodeType[] episodes) {
        this.episodes = episodes;
    }


    /**
     * Gets the unitNumber value for this LocomotiveType.
     * 
     * @return unitNumber
     */
    public int getUnitNumber() {
        return unitNumber;
    }


    /**
     * Sets the unitNumber value for this LocomotiveType.
     * 
     * @param unitNumber
     */
    public void setUnitNumber(int unitNumber) {
        this.unitNumber = unitNumber;
    }


    /**
     * Gets the unitInitial value for this LocomotiveType.
     * 
     * @return unitInitial
     */
    public java.lang.String getUnitInitial() {
        return unitInitial;
    }


    /**
     * Sets the unitInitial value for this LocomotiveType.
     * 
     * @param unitInitial
     */
    public void setUnitInitial(java.lang.String unitInitial) {
        this.unitInitial = unitInitial;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocomotiveType)) return false;
        LocomotiveType other = (LocomotiveType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errors==null && other.getErrors()==null) || 
             (this.errors!=null &&
              java.util.Arrays.equals(this.errors, other.getErrors()))) &&
            ((this.positions==null && other.getPositions()==null) || 
             (this.positions!=null &&
              java.util.Arrays.equals(this.positions, other.getPositions()))) &&
            ((this.refuels==null && other.getRefuels()==null) || 
             (this.refuels!=null &&
              java.util.Arrays.equals(this.refuels, other.getRefuels()))) &&
            ((this.episodes==null && other.getEpisodes()==null) || 
             (this.episodes!=null &&
              java.util.Arrays.equals(this.episodes, other.getEpisodes()))) &&
            this.unitNumber == other.getUnitNumber() &&
            ((this.unitInitial==null && other.getUnitInitial()==null) || 
             (this.unitInitial!=null &&
              this.unitInitial.equals(other.getUnitInitial())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrors() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErrors());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErrors(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPositions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPositions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPositions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRefuels() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRefuels());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRefuels(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEpisodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEpisodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEpisodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getUnitNumber();
        if (getUnitInitial() != null) {
            _hashCode += getUnitInitial().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocomotiveType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "LocomotiveType"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("unitNumber");
        attrField.setXmlName(new javax.xml.namespace.QName("", "UnitNumber"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("unitInitial");
        attrField.setXmlName(new javax.xml.namespace.QName("", "UnitInitial"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errors");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Errors"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "ErrorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Error"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("positions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Positions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "PositionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Position"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refuels");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Refuels"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "RefuelType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Refuel"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("episodes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Episodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "EpisodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.witronix.com/WebServices/v002.003.000", "Episode"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
