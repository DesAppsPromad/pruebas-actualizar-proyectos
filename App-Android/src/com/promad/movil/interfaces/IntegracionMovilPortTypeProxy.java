package com.promad.movil.interfaces;

public class IntegracionMovilPortTypeProxy implements com.promad.movil.interfaces.IntegracionMovilPortType {
  private String _endpoint = null;
  private com.promad.movil.interfaces.IntegracionMovilPortType integracionMovilPortType = null;
  
  public IntegracionMovilPortTypeProxy() {
    _initIntegracionMovilPortTypeProxy();
  }
  
  public IntegracionMovilPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initIntegracionMovilPortTypeProxy();
  }
  
  private void _initIntegracionMovilPortTypeProxy() {
    try {
      integracionMovilPortType = (new com.promad.movil.interfaces.IntegracionMovilServiceLocator()).getIntegracionMovilPort();
      if (integracionMovilPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)integracionMovilPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)integracionMovilPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (integracionMovilPortType != null)
      ((javax.xml.rpc.Stub)integracionMovilPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.promad.movil.interfaces.IntegracionMovilPortType getIntegracionMovilPortType() {
    if (integracionMovilPortType == null)
      _initIntegracionMovilPortTypeProxy();
    return integracionMovilPortType;
  }
  
  public java.lang.Object[] generarEmergencia(java.lang.String in0, java.lang.String in1, long in2, java.lang.String in3, java.lang.String in4, java.lang.String in5, java.lang.String in6, java.lang.String in7, java.lang.String in8, java.lang.String in9, java.lang.String in10, java.lang.String in11, java.lang.String in12, java.lang.String in13, java.lang.String in14, java.lang.String in15, java.lang.String in16, java.lang.String in17, com.promad.movil.interfaces.IntegracionMovilInterface_ItemVehiculo[] in18, com.promad.movil.interfaces.IntegracionMovilInterface_ItemPersona[] in19, java.lang.String in20) throws java.rmi.RemoteException{
    if (integracionMovilPortType == null)
      _initIntegracionMovilPortTypeProxy();
    return integracionMovilPortType.generarEmergencia(in0, in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20);
  }
  
  public java.lang.Object[] notificaMedioAsociado(java.lang.String uuid, java.lang.String folio, java.lang.String nombreMedio, java.lang.String pathURL, java.lang.String tipoMedio, java.lang.String estatusMedio) throws java.rmi.RemoteException{
    if (integracionMovilPortType == null)
      _initIntegracionMovilPortTypeProxy();
    return integracionMovilPortType.notificaMedioAsociado(uuid, folio, nombreMedio, pathURL, tipoMedio, estatusMedio);
  }
  
  
}