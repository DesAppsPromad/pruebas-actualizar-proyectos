/**
 * IntegracionMovilService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.promad.movil.interfaces;

public interface IntegracionMovilService extends javax.xml.rpc.Service {
    public java.lang.String getIntegracionMovilPortAddress();

    public com.promad.movil.interfaces.IntegracionMovilPortType getIntegracionMovilPort() throws javax.xml.rpc.ServiceException;

    public com.promad.movil.interfaces.IntegracionMovilPortType getIntegracionMovilPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
