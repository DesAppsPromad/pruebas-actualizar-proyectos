/**
 * IntegracionMovilServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.promad.movil.interfaces;

public class IntegracionMovilServiceLocator extends org.apache.axis.client.Service implements com.promad.movil.interfaces.IntegracionMovilService {

    public IntegracionMovilServiceLocator() {
    }


    public IntegracionMovilServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IntegracionMovilServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IntegracionMovilPort
    private java.lang.String IntegracionMovilPort_address = "http://services-promad.dyndns.biz:8097/IntegracionMovil/services/IntegracionMovilPort";

    public java.lang.String getIntegracionMovilPortAddress() {
        return IntegracionMovilPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IntegracionMovilPortWSDDServiceName = "IntegracionMovilPort";

    public java.lang.String getIntegracionMovilPortWSDDServiceName() {
        return IntegracionMovilPortWSDDServiceName;
    }

    public void setIntegracionMovilPortWSDDServiceName(java.lang.String name) {
        IntegracionMovilPortWSDDServiceName = name;
    }

    public com.promad.movil.interfaces.IntegracionMovilPortType getIntegracionMovilPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IntegracionMovilPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIntegracionMovilPort(endpoint);
    }

    public com.promad.movil.interfaces.IntegracionMovilPortType getIntegracionMovilPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.promad.movil.interfaces.IntegracionMovilPortSoapBindingStub _stub = new com.promad.movil.interfaces.IntegracionMovilPortSoapBindingStub(portAddress, this);
            _stub.setPortName(getIntegracionMovilPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIntegracionMovilPortEndpointAddress(java.lang.String address) {
        IntegracionMovilPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.promad.movil.interfaces.IntegracionMovilPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.promad.movil.interfaces.IntegracionMovilPortSoapBindingStub _stub = new com.promad.movil.interfaces.IntegracionMovilPortSoapBindingStub(new java.net.URL(IntegracionMovilPort_address), this);
                _stub.setPortName(getIntegracionMovilPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IntegracionMovilPort".equals(inputPortName)) {
            return getIntegracionMovilPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://movil.promad.com/interfaces", "IntegracionMovilService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://movil.promad.com/interfaces", "IntegracionMovilPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IntegracionMovilPort".equals(portName)) {
            setIntegracionMovilPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
