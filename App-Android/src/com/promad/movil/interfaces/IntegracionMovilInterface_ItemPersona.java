/**
 * IntegracionMovilInterface_ItemPersona.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.promad.movil.interfaces;

public class IntegracionMovilInterface_ItemPersona  implements java.io.Serializable {
    private java.lang.String nombrePersona;

    private java.lang.String apellidoPaternoPersona;

    private java.lang.String apellidoMaternoPersona;

    private java.lang.String alias;

    private java.lang.String edad;

    private java.lang.String sexo;

    private java.lang.String complexion;

    private java.lang.String tez;

    private java.lang.String tipoCabello;

    private java.lang.String colorCabello;

    private java.lang.String colorOjos;

    private java.lang.String cara;

    private java.lang.String nariz;

    private java.lang.String cabeza;

    private java.lang.String colorCabeza;

    private java.lang.String talle;

    private java.lang.String colorTalle;

    private java.lang.String inferiores;

    private java.lang.String colorInferiores;

    private java.lang.String calzado;

    private java.lang.String colorCalzado;

    private java.lang.String caracteristicas;

    public IntegracionMovilInterface_ItemPersona() {
    }

    public IntegracionMovilInterface_ItemPersona(
           java.lang.String nombrePersona,
           java.lang.String apellidoPaternoPersona,
           java.lang.String apellidoMaternoPersona,
           java.lang.String alias,
           java.lang.String edad,
           java.lang.String sexo,
           java.lang.String complexion,
           java.lang.String tez,
           java.lang.String tipoCabello,
           java.lang.String colorCabello,
           java.lang.String colorOjos,
           java.lang.String cara,
           java.lang.String nariz,
           java.lang.String cabeza,
           java.lang.String colorCabeza,
           java.lang.String talle,
           java.lang.String colorTalle,
           java.lang.String inferiores,
           java.lang.String colorInferiores,
           java.lang.String calzado,
           java.lang.String colorCalzado,
           java.lang.String caracteristicas) {
           this.nombrePersona = nombrePersona;
           this.apellidoPaternoPersona = apellidoPaternoPersona;
           this.apellidoMaternoPersona = apellidoMaternoPersona;
           this.alias = alias;
           this.edad = edad;
           this.sexo = sexo;
           this.complexion = complexion;
           this.tez = tez;
           this.tipoCabello = tipoCabello;
           this.colorCabello = colorCabello;
           this.colorOjos = colorOjos;
           this.cara = cara;
           this.nariz = nariz;
           this.cabeza = cabeza;
           this.colorCabeza = colorCabeza;
           this.talle = talle;
           this.colorTalle = colorTalle;
           this.inferiores = inferiores;
           this.colorInferiores = colorInferiores;
           this.calzado = calzado;
           this.colorCalzado = colorCalzado;
           this.caracteristicas = caracteristicas;
    }


    /**
     * Gets the nombrePersona value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return nombrePersona
     */
    public java.lang.String getNombrePersona() {
        return nombrePersona;
    }


    /**
     * Sets the nombrePersona value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param nombrePersona
     */
    public void setNombrePersona(java.lang.String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }


    /**
     * Gets the apellidoPaternoPersona value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return apellidoPaternoPersona
     */
    public java.lang.String getApellidoPaternoPersona() {
        return apellidoPaternoPersona;
    }


    /**
     * Sets the apellidoPaternoPersona value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param apellidoPaternoPersona
     */
    public void setApellidoPaternoPersona(java.lang.String apellidoPaternoPersona) {
        this.apellidoPaternoPersona = apellidoPaternoPersona;
    }


    /**
     * Gets the apellidoMaternoPersona value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return apellidoMaternoPersona
     */
    public java.lang.String getApellidoMaternoPersona() {
        return apellidoMaternoPersona;
    }


    /**
     * Sets the apellidoMaternoPersona value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param apellidoMaternoPersona
     */
    public void setApellidoMaternoPersona(java.lang.String apellidoMaternoPersona) {
        this.apellidoMaternoPersona = apellidoMaternoPersona;
    }


    /**
     * Gets the alias value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return alias
     */
    public java.lang.String getAlias() {
        return alias;
    }


    /**
     * Sets the alias value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param alias
     */
    public void setAlias(java.lang.String alias) {
        this.alias = alias;
    }


    /**
     * Gets the edad value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return edad
     */
    public java.lang.String getEdad() {
        return edad;
    }


    /**
     * Sets the edad value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param edad
     */
    public void setEdad(java.lang.String edad) {
        this.edad = edad;
    }


    /**
     * Gets the sexo value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return sexo
     */
    public java.lang.String getSexo() {
        return sexo;
    }


    /**
     * Sets the sexo value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param sexo
     */
    public void setSexo(java.lang.String sexo) {
        this.sexo = sexo;
    }


    /**
     * Gets the complexion value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return complexion
     */
    public java.lang.String getComplexion() {
        return complexion;
    }


    /**
     * Sets the complexion value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param complexion
     */
    public void setComplexion(java.lang.String complexion) {
        this.complexion = complexion;
    }


    /**
     * Gets the tez value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return tez
     */
    public java.lang.String getTez() {
        return tez;
    }


    /**
     * Sets the tez value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param tez
     */
    public void setTez(java.lang.String tez) {
        this.tez = tez;
    }


    /**
     * Gets the tipoCabello value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return tipoCabello
     */
    public java.lang.String getTipoCabello() {
        return tipoCabello;
    }


    /**
     * Sets the tipoCabello value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param tipoCabello
     */
    public void setTipoCabello(java.lang.String tipoCabello) {
        this.tipoCabello = tipoCabello;
    }


    /**
     * Gets the colorCabello value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return colorCabello
     */
    public java.lang.String getColorCabello() {
        return colorCabello;
    }


    /**
     * Sets the colorCabello value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param colorCabello
     */
    public void setColorCabello(java.lang.String colorCabello) {
        this.colorCabello = colorCabello;
    }


    /**
     * Gets the colorOjos value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return colorOjos
     */
    public java.lang.String getColorOjos() {
        return colorOjos;
    }


    /**
     * Sets the colorOjos value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param colorOjos
     */
    public void setColorOjos(java.lang.String colorOjos) {
        this.colorOjos = colorOjos;
    }


    /**
     * Gets the cara value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return cara
     */
    public java.lang.String getCara() {
        return cara;
    }


    /**
     * Sets the cara value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param cara
     */
    public void setCara(java.lang.String cara) {
        this.cara = cara;
    }


    /**
     * Gets the nariz value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return nariz
     */
    public java.lang.String getNariz() {
        return nariz;
    }


    /**
     * Sets the nariz value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param nariz
     */
    public void setNariz(java.lang.String nariz) {
        this.nariz = nariz;
    }


    /**
     * Gets the cabeza value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return cabeza
     */
    public java.lang.String getCabeza() {
        return cabeza;
    }


    /**
     * Sets the cabeza value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param cabeza
     */
    public void setCabeza(java.lang.String cabeza) {
        this.cabeza = cabeza;
    }


    /**
     * Gets the colorCabeza value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return colorCabeza
     */
    public java.lang.String getColorCabeza() {
        return colorCabeza;
    }


    /**
     * Sets the colorCabeza value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param colorCabeza
     */
    public void setColorCabeza(java.lang.String colorCabeza) {
        this.colorCabeza = colorCabeza;
    }


    /**
     * Gets the talle value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return talle
     */
    public java.lang.String getTalle() {
        return talle;
    }


    /**
     * Sets the talle value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param talle
     */
    public void setTalle(java.lang.String talle) {
        this.talle = talle;
    }


    /**
     * Gets the colorTalle value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return colorTalle
     */
    public java.lang.String getColorTalle() {
        return colorTalle;
    }


    /**
     * Sets the colorTalle value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param colorTalle
     */
    public void setColorTalle(java.lang.String colorTalle) {
        this.colorTalle = colorTalle;
    }


    /**
     * Gets the inferiores value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return inferiores
     */
    public java.lang.String getInferiores() {
        return inferiores;
    }


    /**
     * Sets the inferiores value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param inferiores
     */
    public void setInferiores(java.lang.String inferiores) {
        this.inferiores = inferiores;
    }


    /**
     * Gets the colorInferiores value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return colorInferiores
     */
    public java.lang.String getColorInferiores() {
        return colorInferiores;
    }


    /**
     * Sets the colorInferiores value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param colorInferiores
     */
    public void setColorInferiores(java.lang.String colorInferiores) {
        this.colorInferiores = colorInferiores;
    }


    /**
     * Gets the calzado value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return calzado
     */
    public java.lang.String getCalzado() {
        return calzado;
    }


    /**
     * Sets the calzado value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param calzado
     */
    public void setCalzado(java.lang.String calzado) {
        this.calzado = calzado;
    }


    /**
     * Gets the colorCalzado value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return colorCalzado
     */
    public java.lang.String getColorCalzado() {
        return colorCalzado;
    }


    /**
     * Sets the colorCalzado value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param colorCalzado
     */
    public void setColorCalzado(java.lang.String colorCalzado) {
        this.colorCalzado = colorCalzado;
    }


    /**
     * Gets the caracteristicas value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @return caracteristicas
     */
    public java.lang.String getCaracteristicas() {
        return caracteristicas;
    }


    /**
     * Sets the caracteristicas value for this IntegracionMovilInterface_ItemPersona.
     * 
     * @param caracteristicas
     */
    public void setCaracteristicas(java.lang.String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IntegracionMovilInterface_ItemPersona)) return false;
        IntegracionMovilInterface_ItemPersona other = (IntegracionMovilInterface_ItemPersona) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nombrePersona==null && other.getNombrePersona()==null) || 
             (this.nombrePersona!=null &&
              this.nombrePersona.equals(other.getNombrePersona()))) &&
            ((this.apellidoPaternoPersona==null && other.getApellidoPaternoPersona()==null) || 
             (this.apellidoPaternoPersona!=null &&
              this.apellidoPaternoPersona.equals(other.getApellidoPaternoPersona()))) &&
            ((this.apellidoMaternoPersona==null && other.getApellidoMaternoPersona()==null) || 
             (this.apellidoMaternoPersona!=null &&
              this.apellidoMaternoPersona.equals(other.getApellidoMaternoPersona()))) &&
            ((this.alias==null && other.getAlias()==null) || 
             (this.alias!=null &&
              this.alias.equals(other.getAlias()))) &&
            ((this.edad==null && other.getEdad()==null) || 
             (this.edad!=null &&
              this.edad.equals(other.getEdad()))) &&
            ((this.sexo==null && other.getSexo()==null) || 
             (this.sexo!=null &&
              this.sexo.equals(other.getSexo()))) &&
            ((this.complexion==null && other.getComplexion()==null) || 
             (this.complexion!=null &&
              this.complexion.equals(other.getComplexion()))) &&
            ((this.tez==null && other.getTez()==null) || 
             (this.tez!=null &&
              this.tez.equals(other.getTez()))) &&
            ((this.tipoCabello==null && other.getTipoCabello()==null) || 
             (this.tipoCabello!=null &&
              this.tipoCabello.equals(other.getTipoCabello()))) &&
            ((this.colorCabello==null && other.getColorCabello()==null) || 
             (this.colorCabello!=null &&
              this.colorCabello.equals(other.getColorCabello()))) &&
            ((this.colorOjos==null && other.getColorOjos()==null) || 
             (this.colorOjos!=null &&
              this.colorOjos.equals(other.getColorOjos()))) &&
            ((this.cara==null && other.getCara()==null) || 
             (this.cara!=null &&
              this.cara.equals(other.getCara()))) &&
            ((this.nariz==null && other.getNariz()==null) || 
             (this.nariz!=null &&
              this.nariz.equals(other.getNariz()))) &&
            ((this.cabeza==null && other.getCabeza()==null) || 
             (this.cabeza!=null &&
              this.cabeza.equals(other.getCabeza()))) &&
            ((this.colorCabeza==null && other.getColorCabeza()==null) || 
             (this.colorCabeza!=null &&
              this.colorCabeza.equals(other.getColorCabeza()))) &&
            ((this.talle==null && other.getTalle()==null) || 
             (this.talle!=null &&
              this.talle.equals(other.getTalle()))) &&
            ((this.colorTalle==null && other.getColorTalle()==null) || 
             (this.colorTalle!=null &&
              this.colorTalle.equals(other.getColorTalle()))) &&
            ((this.inferiores==null && other.getInferiores()==null) || 
             (this.inferiores!=null &&
              this.inferiores.equals(other.getInferiores()))) &&
            ((this.colorInferiores==null && other.getColorInferiores()==null) || 
             (this.colorInferiores!=null &&
              this.colorInferiores.equals(other.getColorInferiores()))) &&
            ((this.calzado==null && other.getCalzado()==null) || 
             (this.calzado!=null &&
              this.calzado.equals(other.getCalzado()))) &&
            ((this.colorCalzado==null && other.getColorCalzado()==null) || 
             (this.colorCalzado!=null &&
              this.colorCalzado.equals(other.getColorCalzado()))) &&
            ((this.caracteristicas==null && other.getCaracteristicas()==null) || 
             (this.caracteristicas!=null &&
              this.caracteristicas.equals(other.getCaracteristicas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNombrePersona() != null) {
            _hashCode += getNombrePersona().hashCode();
        }
        if (getApellidoPaternoPersona() != null) {
            _hashCode += getApellidoPaternoPersona().hashCode();
        }
        if (getApellidoMaternoPersona() != null) {
            _hashCode += getApellidoMaternoPersona().hashCode();
        }
        if (getAlias() != null) {
            _hashCode += getAlias().hashCode();
        }
        if (getEdad() != null) {
            _hashCode += getEdad().hashCode();
        }
        if (getSexo() != null) {
            _hashCode += getSexo().hashCode();
        }
        if (getComplexion() != null) {
            _hashCode += getComplexion().hashCode();
        }
        if (getTez() != null) {
            _hashCode += getTez().hashCode();
        }
        if (getTipoCabello() != null) {
            _hashCode += getTipoCabello().hashCode();
        }
        if (getColorCabello() != null) {
            _hashCode += getColorCabello().hashCode();
        }
        if (getColorOjos() != null) {
            _hashCode += getColorOjos().hashCode();
        }
        if (getCara() != null) {
            _hashCode += getCara().hashCode();
        }
        if (getNariz() != null) {
            _hashCode += getNariz().hashCode();
        }
        if (getCabeza() != null) {
            _hashCode += getCabeza().hashCode();
        }
        if (getColorCabeza() != null) {
            _hashCode += getColorCabeza().hashCode();
        }
        if (getTalle() != null) {
            _hashCode += getTalle().hashCode();
        }
        if (getColorTalle() != null) {
            _hashCode += getColorTalle().hashCode();
        }
        if (getInferiores() != null) {
            _hashCode += getInferiores().hashCode();
        }
        if (getColorInferiores() != null) {
            _hashCode += getColorInferiores().hashCode();
        }
        if (getCalzado() != null) {
            _hashCode += getCalzado().hashCode();
        }
        if (getColorCalzado() != null) {
            _hashCode += getColorCalzado().hashCode();
        }
        if (getCaracteristicas() != null) {
            _hashCode += getCaracteristicas().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IntegracionMovilInterface_ItemPersona.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://movil.promad.com/interfaces", "IntegracionMovilInterface_ItemPersona"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombrePersona");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombrePersona"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apellidoPaternoPersona");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apellidoPaternoPersona"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apellidoMaternoPersona");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apellidoMaternoPersona"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alias");
        elemField.setXmlName(new javax.xml.namespace.QName("", "alias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "edad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sexo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sexo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("complexion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "complexion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tez");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tez"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCabello");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoCabello"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colorCabello");
        elemField.setXmlName(new javax.xml.namespace.QName("", "colorCabello"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colorOjos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "colorOjos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cara");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cara"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nariz");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nariz"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cabeza");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cabeza"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colorCabeza");
        elemField.setXmlName(new javax.xml.namespace.QName("", "colorCabeza"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("talle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "talle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colorTalle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "colorTalle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inferiores");
        elemField.setXmlName(new javax.xml.namespace.QName("", "inferiores"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colorInferiores");
        elemField.setXmlName(new javax.xml.namespace.QName("", "colorInferiores"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calzado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calzado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colorCalzado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "colorCalzado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("caracteristicas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "caracteristicas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
