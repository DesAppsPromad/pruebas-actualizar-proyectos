package com.promad.movil.interfaces;

import java.util.Arrays;

public class IgenerarEmergenciaResponse  implements java.io.Serializable {

	String[] generarEmergenciaReturn = new String[3];

	public void IgenerarEmergenciaResponse() {
		generarEmergenciaReturn[0] = "";
		generarEmergenciaReturn[1] = "";
		generarEmergenciaReturn[2] = "";
	}
	
	public String[] getGenerarEmergenciaReturn() {
		return generarEmergenciaReturn;
	}

	public void setGenerarEmergenciaReturn(String[] generarEmergenciaReturn) {
		this.generarEmergenciaReturn = generarEmergenciaReturn;
	}

	@Override
	public String toString() {
		return "IgenerarEmergenciaResponse [generarEmergenciaReturn=" + Arrays.toString(generarEmergenciaReturn) + "]";
	}
	
}
