/**
 * IntegracionMovilInterface_ItemVehiculo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.promad.movil.interfaces;

public class IntegracionMovilInterface_ItemVehiculo  implements java.io.Serializable {
    private java.lang.String tipoVehiculo;

    private java.lang.String nombreMarca;

    private java.lang.String nombreModelo;

    private java.lang.String placa;

    private java.lang.String noSerie;

    private java.lang.String anio;

    private java.lang.String detalles;

    public IntegracionMovilInterface_ItemVehiculo() {
    }

    public IntegracionMovilInterface_ItemVehiculo(
           java.lang.String tipoVehiculo,
           java.lang.String nombreMarca,
           java.lang.String nombreModelo,
           java.lang.String placa,
           java.lang.String noSerie,
           java.lang.String anio,
           java.lang.String detalles) {
           this.tipoVehiculo = tipoVehiculo;
           this.nombreMarca = nombreMarca;
           this.nombreModelo = nombreModelo;
           this.placa = placa;
           this.noSerie = noSerie;
           this.anio = anio;
           this.detalles = detalles;
    }


    /**
     * Gets the tipoVehiculo value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @return tipoVehiculo
     */
    public java.lang.String getTipoVehiculo() {
        return tipoVehiculo;
    }


    /**
     * Sets the tipoVehiculo value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @param tipoVehiculo
     */
    public void setTipoVehiculo(java.lang.String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }


    /**
     * Gets the nombreMarca value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @return nombreMarca
     */
    public java.lang.String getNombreMarca() {
        return nombreMarca;
    }


    /**
     * Sets the nombreMarca value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @param nombreMarca
     */
    public void setNombreMarca(java.lang.String nombreMarca) {
        this.nombreMarca = nombreMarca;
    }


    /**
     * Gets the nombreModelo value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @return nombreModelo
     */
    public java.lang.String getNombreModelo() {
        return nombreModelo;
    }


    /**
     * Sets the nombreModelo value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @param nombreModelo
     */
    public void setNombreModelo(java.lang.String nombreModelo) {
        this.nombreModelo = nombreModelo;
    }


    /**
     * Gets the placa value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @return placa
     */
    public java.lang.String getPlaca() {
        return placa;
    }


    /**
     * Sets the placa value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @param placa
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }


    /**
     * Gets the noSerie value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @return noSerie
     */
    public java.lang.String getNoSerie() {
        return noSerie;
    }


    /**
     * Sets the noSerie value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @param noSerie
     */
    public void setNoSerie(java.lang.String noSerie) {
        this.noSerie = noSerie;
    }


    /**
     * Gets the anio value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @return anio
     */
    public java.lang.String getAnio() {
        return anio;
    }


    /**
     * Sets the anio value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @param anio
     */
    public void setAnio(java.lang.String anio) {
        this.anio = anio;
    }


    /**
     * Gets the detalles value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @return detalles
     */
    public java.lang.String getDetalles() {
        return detalles;
    }


    /**
     * Sets the detalles value for this IntegracionMovilInterface_ItemVehiculo.
     * 
     * @param detalles
     */
    public void setDetalles(java.lang.String detalles) {
        this.detalles = detalles;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IntegracionMovilInterface_ItemVehiculo)) return false;
        IntegracionMovilInterface_ItemVehiculo other = (IntegracionMovilInterface_ItemVehiculo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipoVehiculo==null && other.getTipoVehiculo()==null) || 
             (this.tipoVehiculo!=null &&
              this.tipoVehiculo.equals(other.getTipoVehiculo()))) &&
            ((this.nombreMarca==null && other.getNombreMarca()==null) || 
             (this.nombreMarca!=null &&
              this.nombreMarca.equals(other.getNombreMarca()))) &&
            ((this.nombreModelo==null && other.getNombreModelo()==null) || 
             (this.nombreModelo!=null &&
              this.nombreModelo.equals(other.getNombreModelo()))) &&
            ((this.placa==null && other.getPlaca()==null) || 
             (this.placa!=null &&
              this.placa.equals(other.getPlaca()))) &&
            ((this.noSerie==null && other.getNoSerie()==null) || 
             (this.noSerie!=null &&
              this.noSerie.equals(other.getNoSerie()))) &&
            ((this.anio==null && other.getAnio()==null) || 
             (this.anio!=null &&
              this.anio.equals(other.getAnio()))) &&
            ((this.detalles==null && other.getDetalles()==null) || 
             (this.detalles!=null &&
              this.detalles.equals(other.getDetalles())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipoVehiculo() != null) {
            _hashCode += getTipoVehiculo().hashCode();
        }
        if (getNombreMarca() != null) {
            _hashCode += getNombreMarca().hashCode();
        }
        if (getNombreModelo() != null) {
            _hashCode += getNombreModelo().hashCode();
        }
        if (getPlaca() != null) {
            _hashCode += getPlaca().hashCode();
        }
        if (getNoSerie() != null) {
            _hashCode += getNoSerie().hashCode();
        }
        if (getAnio() != null) {
            _hashCode += getAnio().hashCode();
        }
        if (getDetalles() != null) {
            _hashCode += getDetalles().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IntegracionMovilInterface_ItemVehiculo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://movil.promad.com/interfaces", "IntegracionMovilInterface_ItemVehiculo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoVehiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoVehiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreMarca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombreMarca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreModelo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombreModelo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("placa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "placa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("noSerie");
        elemField.setXmlName(new javax.xml.namespace.QName("", "noSerie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "anio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detalles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
