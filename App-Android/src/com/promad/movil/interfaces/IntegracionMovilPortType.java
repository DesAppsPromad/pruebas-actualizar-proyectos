/**
 * IntegracionMovilPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.promad.movil.interfaces;

public interface IntegracionMovilPortType extends java.rmi.Remote {
    public java.lang.Object[] generarEmergencia(java.lang.String in0, java.lang.String in1, long in2, java.lang.String in3, java.lang.String in4, java.lang.String in5, java.lang.String in6, java.lang.String in7, java.lang.String in8, java.lang.String in9, java.lang.String in10, java.lang.String in11, java.lang.String in12, java.lang.String in13, java.lang.String in14, java.lang.String in15, java.lang.String in16, java.lang.String in17, com.promad.movil.interfaces.IntegracionMovilInterface_ItemVehiculo[] in18, com.promad.movil.interfaces.IntegracionMovilInterface_ItemPersona[] in19, java.lang.String in20) throws java.rmi.RemoteException;
    public java.lang.Object[] notificaMedioAsociado(java.lang.String uuid, java.lang.String folio, java.lang.String nombreMedio, java.lang.String pathURL, java.lang.String tipoMedio, java.lang.String estatusMedio) throws java.rmi.RemoteException;
}
